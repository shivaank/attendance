<?php

require 'db.php';

$JSONstr ='{
                "RollNo":"15BCS0040",
                "SNo":"ZY22263H56"
           }';
$JSONstr = file_get_contents('php://input');

$RollNo =  json_decode($JSONstr)->RollNo;

class resp {
    function resp(){
        $this->Name = null;
        $this->RollNo = null;
        $this->EnrollmentNo = null;
        $this->Department = null;
        $this->SubjectCode = array();
        $this->SubjectName = array();
        $this->Semester = null;
        $this->error = null;
        $this->error_msg = null;
    }
}

$Object = json_decode($JSONstr);

$response = new resp();
$SNo = $Object->SNo;
$Roll =  strtoupper($Object->RollNo);

$query = "SELECT SNo, EnrollmentNo, Email FROM StudentBase WHERE RollNo = '$RollNo' ";
$result = $conn->query($query);
$row = $result->fetch_assoc();

if($row['SNo'] == NULL && $row['Email'] != NULL && $row['EnrollmentNo'] != NULL  ) {

    $query = "SELECT * FROM StudentBase WHERE RollNo = '$Roll'";
    if ($conn->query($query)) {
        $result = $conn->query($query);
        $row = $result->fetch_assoc();
        if ($result->num_rows == 0) {
            $response->error = "1";
            $response->error_msg = "This Roll number is not registered. Please select the option 'Sign up' on the home page and sign up with your correct details first.";
            echo json_encode($response);
        } else {
 $query2 = "SELECT * FROM StudentBase WHERE SNo = '$SNo'";
                $res2 = $conn->query($query2);
                if($res2->num_rows > 0)
                {
                    $response->error = "1";
                    $response->error_msg = "This device is already in use by another student. Couldn't assign you this phone.";
                    echo json_encode($response);
                    exit(0);
                }
                $response->error = "0";
                if ($row['MiddleName'] != null)
                    $response->Name = $row['FirstName'] . ' ' . $row['MiddleName'] . ' ' . $row['LastName'];
                else
                    $response->Name = $row['FirstName'] . ' ' . $row['LastName'];

                $response->RollNo = $row['RollNo'];
                $response->EnrollmentNo = $row['EnrollmentNo'];
                $DeptId = $row['DeptId'];

                $qDept = "SELECT Name FROM Department WHERE DeptId = '$DeptId'";
                $res = $conn->query($qDept);
                $rowD = $res->fetch_assoc();
                $response->Department = $rowD['Name'];

                $response->Semester = $row['Semester'];
                $response->error_msg = "Welcome " . $row['FirstName'] . ' ' . $row['MiddleName'];

                $Sem = $response->Semester;
                $qSub = "SELECT SubjectCode, SubjectName FROM Subjects WHERE Semester = '$Sem'";
                $resSub = $conn->query($qSub);
                $j = 0;
                if ($resSub) {
                    while ($rowSub = $resSub->fetch_assoc()) {
                        $response->SubjectName[$j] = $rowSub['SubjectName'];
                        $response->SubjectCode[$j++] = $rowSub['SubjectCode'];
                    }
                }
                $query = "UPDATE StudentBase SET SNo = '$SNo' WHERE RollNo = '$RollNo' ";
                $result = $conn->query($query);
                echo json_encode($response);
        }
    } else {
        $response->error = "1";
        $response->error_msg = "Internal server error. Shame on us";
        echo json_encode($response);
    }
}
else{
    $response->error = "1";
    $response->error_msg = "Seems like you haven't logged out successfully from your old phone. Couldn't log you in.";
    echo json_encode($response);
}
?>