<?php
include "db.php";

session_start();
if(!isset($_SESSION['status'])){
    $_SESSION['status']=0;
    $status = $_SESSION['status'];
    header('location:index.php');
    exit(0);
}
else{
    $status = $_SESSION['status'];
    if($status == 2){
        header('location:studentDashboard.php');
        echo "$status";
        exit(0);

    }elseif($status == 0){
        header('location:index.php');
        echo "$status";
        exit(0);
    }
    elseif($status == 3){}
    else
    {
        header('location:index.php');
        echo "$status";
        exit(0);
    }
}
$month = $_GET['month'];
$SubjectCode = $_GET['subject'];
$Semester = $_GET['semester'];
if($month < 10)
    $month = "0".(string)((int)$month);
else
    $month = (string)$month;
$Month = array("01"=>"January","02"=>"February","03"=>"March","04"=>"April","05"=>"May","06"=>"June","07"=>"July","08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");

$mon = $month;
$month = $Month[$month];



echo "<head>

<meta charset='UTF-8'>
      <meta name='viewport' content='width=device-width, initial-scale=1'>
      <link rel='stylesheet' href='css/roboto.css' type='text/css'>
      <link href='css/MatIco.css' rel='stylesheet'>
      <link href='css/bootstrap.min.css' rel='stylesheet'>
      <link href='css/bootstrap-material-design.css' rel='stylesheet'>
      <link href='css/ripples.min.css' rel='stylesheet'>
      <link href='css/snackbar.css' rel='stylesheet'>
      <link href='css/responsivetable.css' rel='stylesheet'>


<style>
.switch {
  position: relative;
  display: inline-block;
  width: 30px;
  height: 15.75px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .05s;
  transition: .05s;
}

.slider:before {
  position: absolute;
  content: \"\";
  height: 15.75px;
  width: 15.75px;
  left: 0px;
  bottom: 0px;
  background-color: white;
  -webkit-transition: .05s;
  transition: .05s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(14px);
  -ms-transform: translateX(14px);
  transform: translateX(14px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 21px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
</head>
<body>
<div class='bs-component'>
        <div class='navbar navbar-danger'>
            <div class='container-fluid'>
                <div class='navbar-header'>
                    <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-responsive-collapse'>
                  <span class='icon-bar'></span>
                  <span class='icon-bar'></span>
                  <span class='icon-bar'></span>
                </button>
                    <a class='navbar-brand' href='index.php'>AM</a>
                </div>
                <div class='navbar-collapse collapse navbar-responsive-collapse'>
                <!--    <ul class='nav navbar-nav navbar-right'>
                        <li><a href='signUPForm.php'>SignUP <span class='glyphicon glyphicon-ok'></span><div class='ripple-container'></div></a></li>
                        <li><a href='login.php'>LogIn <span class='glyphicon glyphicon-log-in'></span><div class='ripple-container'></div></a></li>
                    </ul> -->
                    <ul class='nav navbar-nav navbar-right'>
                              <li><a href='test.php'>LogOut <span class='glyphicon glyphicon-log-out'></span><div class='ripple-container'></div></a></li>
                          </ul>
                </div>
            </div>
        </div>
    </div>
    <div class='jumbotron' id ='class_list_uneditable'>
        <div class='container-fluid'>
            <div class='col-md-2'></div>
            <div class='col-md-8 col-sm-10 col-xs-9'>";


if($Semester == 0)
{
    echo "<center> <h2>You need to select a Semester and a Subject.</h2> </center>";
}
else if($SubjectCode =='0')
{
    echo "<center> <h2>Please select a Subject.</h2> </center>";
}
else {

    echo "<h4><b>" . $month . " Attendance</b></h4>";
    if ($Semester > 8) {
        echo "<h5>M. Tech </h5>";
        echo "<h5>Semester : " . ($Semester - 8) . "</h5>";
    }
    else
    {
        echo "<h5>B. Tech </h5>";
        echo "<h5>Semester : " . $Semester . " </h5>";
    }
    $query = "SELECT SubjectName FROM Subjects WHERE SubjectCode = '$SubjectCode'";
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    $Subject = $row['SubjectName'];

    echo "<h5>Subject : $Subject</h5><br>";
    echo "<p id = 'para'>  </p>";
    echo "
    <div class='scrolling'>
        <div class='inner' >

           <table class='table table-striped table-hover table-condensed'>";


           //yaha se
    $query = "SELECT COLUMN_NAME 
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = '$SubjectCode' AND TABLE_SCHEMA='$database'";
    $result = $conn->query($query);
    $row = $result->fetch_assoc();

    $TDArray = array(array());
    $NClasses = array();
    $NClasses[0] = ""; $NClasses[1] = "";
    $TDArray[0][0] = "Roll No";
    $TDArray[0][1] = "Name";
    $m = 1;
    $j = 0;
    while($row = $result->fetch_assoc())
    {
        //echo $row['COLUMN_NAME']."<br>";
        if(substr($row['COLUMN_NAME'],4,2) == $mon)
        {
            ++$j;
            $num = (int)substr($row['COLUMN_NAME'],1,2);
            $nClass = (int)substr($row['COLUMN_NAME'],-1);
            $time = substr($row['COLUMN_NAME'],10,2);
            $TDArray[0][++$m] = (string)$num." ($time:00)";
            $NClasses[$m] = $nClass;
            //$TDArray[0][$m] .=  "(".T($nClass).")";
            break;
        }
        else
            ++$j;
    }
    $first = $j;
    while($row = $result->fetch_assoc())
    {
        //echo $row['COLUMN_NAME']."<br>";
        if(substr($row['COLUMN_NAME'],4,2) == $mon)
        {
            //$TDArray[0][++$m] = (int)substr($row['COLUMN_NAME'],1,2);
            ++$j;
            $num = (int)substr($row['COLUMN_NAME'],1,2);
            $nClass = (int)substr($row['COLUMN_NAME'],-1);
            $time = substr($row['COLUMN_NAME'],10,2);
            $TDArray[0][++$m] = (string)$num." ($time:00)";
            $NClasses[$m] = $nClass;
            //$TDArray[0][$m] .= "(".T($nClass).")";
        }
        else
            break;
    }
    $n =0;$m = 1;
    $query = "SELECT * FROM $SubjectCode";
    $result = $conn->query($query);
    while($row = $result->fetch_assoc())
    {
        $m = 1;
        $row = array_values($row);
        $TDArray[++$n][0] = $row[0];

        $qName = "SELECT FirstName, MiddleName, LastName FROM StudentBase WHERE RollNo = '$row[0]'";
        $rName = $conn->query($qName)->fetch_assoc();

        if($rName['MiddleName'] != null)
            $TDArray[$n][1] = $rName['FirstName']." ".$rName['MiddleName']." ".$rName['LastName'];
        else
            $TDArray[$n][1] = $rName['FirstName']." ".$rName['LastName'];

        for($i = $first;$i<=$j;$i++)
        {
            $TDArray[$n][++$m] = $row[$i];
        }
    }
    // echo $n." ".$m."<br>";
    echo "<tr>";
    echo "<th align='left'>";
    echo $TDArray[0][0];
    echo "</th>";
    echo "<td align='left'><b>";
    echo $TDArray[0][1];
    echo "</b></td>";
    for($j=2;$j<=$m;$j++)
    {
        $val = $TDArray[0][$j];
        echo "<td align='center'><b>";
        echo $TDArray[0][$j];
        echo "</b></td>";
    }
    echo "</tr>";
    for($i=1;$i<=$n;$i++)
    {
        echo "<tr>";
        echo "<th align='left'>";
        echo $TDArray[$i][0];
        echo "</th>";
        echo "<td align='left'><b>";
        echo $TDArray[$i][1];
        echo "<b></td>";
        for($j=2;$j<=$m;$j++)
        {
            echo "<td align='center'>";
            $str = "";
            $roll = $TDArray[$i][0]; $col = $TDArray[0][$j];
            $pass = $roll.'_'.$col;

            if($TDArray[$i][$j] == 0)
                echo "<label class='switch'>
                          <input type='checkbox' onclick = 'edit(\"".$pass."\");' id = '$pass'>
                          <span class='slider round'></span>
                        </label>";
            else
                echo "
                        <label class='switch'>
                          <input type='checkbox' checked onclick = 'edit(\"".$pass."\");' id = '$pass'>
                          <span class='slider round'></span>
                        </label>";
           /* for($a = 0;$a<$TDArray[$i][$j];$a++)
                $str .= "P";
            $t = $NClasses[$j] - $TDArray[$i][$j];
            for($a = 0;$a<$t;$a++)
                $str .= "A";

            echo $str;*/
            echo "</td>";
        }
        echo "</tr>";
    }



            // yaha tak
    echo "</table>

</div>
</div>"
    ;
}

/*
echo"

<form method='get' action='pdf.php' target='_blank'>
                    <input type='hidden' name='Code' value = $SubjectCode>
                    <input type='hidden' name ='Mon' value = $mon>
                <input type='SUBMIT' value='Save as PDF' >
                </form>";*/
   echo "             
                <div class='row'>                                                                                       
            <div class='container-fluid'>                                                                   
                        <div class='col-md-2'></div>
                     <div class='col-md-3'>                                                                          
                <a class='btn' role='button' onclick='getExcel()' style='background:#4e6ab2;'>get excel </a>                 
            </div> 
              <form method='post' action='pdf.php' target='_self' class='col-md-3'>
                    <input type='hidden' name='Code' value = $SubjectCode>
                    <input type='hidden' name ='Mon' value = $mon>
                <input type='SUBMIT' value='get PDF' class='btn' role='button' style='background:#d63737;'>
                </form>
                                                                                              
        </div>                                                                                              
    </div> 
                
                
                
                </div>
                </div>
    <script>
    function edit()
    {
        //document.getElementById('para').innerHTML = arguments[0];
        if (window.XMLHttpRequest) {

                      xmlhttp = new XMLHttpRequest();
                  } else {

                      xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
                  }
                  xmlhttp.onreadystatechange = function() {
                      if (this.readyState == 4 && this.status == 200) {
                          document.getElementById('para').innerHTML = this.responseText;
                      }
                  };
                  xmlhttp.open('GET','teacherBackend.php?req='+5+'&Data='+arguments[0]+'&SubjectCode=$SubjectCode&month=$mon',true);
                  xmlhttp.send();
        
        
    }
    
    function getExcel() {
                window.open('exceldoc.php?month=$mon&subject=$SubjectCode&semester=$Semester','_self');

    }
    
    </script>



 <script src='js/jquery-1.10.2.min.js'></script>
  <script src='js/bootstrap.min.js'></script>
  <script src='js/ripples.min.js'></script>
  <script src='js/material.min.js'></script>
  <script src='js/snackbar.min.js'></script>
  <script src='js/jquery.nouislider.min.js'></script>";

$Teacher = $_SESSION['first_name'].$_SESSION['teacher_id'].$_SESSION['Email'];
echo"

<script src='https://www.gstatic.com/firebasejs/4.2.0/firebase.js'></script>     
<script>
  // Initialize Firebase
  var config = {
    apiKey: 'AIzaSyCLd28O-wFDc-uv9Fu02bmKVbJSYp-xZcw',
    authDomain: 'jmizone-e8f77.firebaseapp.com',
    databaseURL: 'https://jmizone-e8f77.firebaseio.com',
    projectId: 'jmizone-e8f77',
    storageBucket: 'jmizone-e8f77.appspot.com',
    messagingSenderId: '760487238037'
  };
  firebase.initializeApp(config);

var firebaseRef = firebase.database().ref();
function detectmob() {
    if( navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
    ){
        return true;
    }
    else {
        return false;
    }
}

if(detectmob() == true)
    firebaseRef.child(Date()+'EditAttendancePHP').set('phone ->$Teacher');
else
    firebaseRef.child(Date()+'EditAttendancePHP').set('PC ->$Teacher');
</script>;


</body>

<footer class=' bscomponent navbar navbar-fixed-bottom navbar-danger'>

    <div class='container-fluid' style='padding:20px;'>
        <div class='row'>
            <div class='col-md-4' style='text-align:center;'>
                <a href='#' style='color:#FFF;'>About Us</a>
            </div>
            <div class='col-md-4' style='text-align:center;color:#FFF;'>
                <a href='#' style='color:#FFF;'>Contact Us</a>
            </div>
            <div class='col-md-4' style='text-align:center;color:#FFF;'>
                <a href='#' style='color:#FFF;'>Team</a>
            </div>
        </div>
    </div>
</footer>



";


?>