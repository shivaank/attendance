<?php

require('./fpdf/fpdf.php');
session_start();
if(!isset($_SESSION['status'])){
    $_SESSION['status']=0;
    $status = $_SESSION['status'];
    header('location:index.php');
    exit(0);
}
else{
    $status = $_SESSION['status'];
    if($status == 2){
        header('location:studentDashboard.php');
        echo "$status";
        exit(0);

    }elseif($status == 0){
        header('location:index.php');
        echo "$status";
        exit(0);
    }
    elseif($status == 3){}
    else
    {
        header('location:index.php');
        echo "$status";
        exit(0);
    }
}
class PDF extends FPDF
{
    function Header()
    {

    }

// Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

// Simple table
    function BasicTable($header, $data, $NameStd, $TotCl,$month,$Year,$Sub,$Semester,$sum,$ClassPresent)
    {
        // Header

        // Logo
        //$this->Image('AppLogo.png', 170, 8, 20);
        $this->Image('JamiaLogo.png', 14, 5, 20);
        // Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        // Move to the right
        $this->Cell(55);
        // Title
        $this->Cell(88, 10,"Computer Engg Department" , 1, 0, 'C');
        // Line break
        $this->Ln(19);

        $this->SetFont('Arial', 'B', 12);
        $this->cell(60,5,$month.' \''.$Year.' Attendance',0);
        $this->SetFont('Arial', '', 11);
        $this->Ln();
        if($Semester > 8) {
            $Semester -= 8;
            $this->cell(60,5,'M.Tech Semester : '.$Semester,0);
            $this->Ln();
        }
        else {
            $this->cell(60, 5, 'B.Tech Semester : ' . $Semester, 0);
            $this->Ln();
        }
        $this->cell(60,5,$Sub,0);
        $this->Cell(90);

        $this->SetFont('Arial', 'B', 11);
        $this->Cell(60,5,'Total Classes = '.$sum);
        $this->Ln(10);

        $this->SetFont('Arial', 'B', 10);
        $n = count($header);
        $this->Cell(9.3,6,$header[0],1);

        $this->Cell(50,6,$header[1],1);
        for($i = 2;$i<$n;$i++) {
            if($TotCl[$i-2] == 1)
                $this->Cell(5.5, 6, $header[$i], 1,0,'C');
            else
                $this->Cell($TotCl[$i-2]*3.6, 6, $header[$i], 1,0,'C');
        }
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(9, 6, 'Tot', 1,0,'C');
        $this->Cell(9, 6, '%', 1,0,'C');
        $this->SetFont('Arial', '', 10);
        $this->Ln();
        // Data
        $m = count($data);

        for($i = 0;$i<$m;$i++) {
            $n = count($data[$i]);
           // $this->Cell(8, 5.2, $row[0], 1);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(9.3, 5.2,substr($data[$i][0],-2), 1);
            $this->SetFont('Arial', '', 10);
            if(strlen($NameStd[$i])>25)
                $NameStd[$i] = substr($NameStd[$i],0,23)."...";

            $this->Cell(50,5.2,$NameStd[$i],1);


            for($j =1;$j<$n;$j++) {
                if(strlen($data[$i][$j]) == 1)
                    $this->Cell(5.5,5.2 , $data[$i][$j], 1,0,'C');
                else
                    $this->Cell(strlen($data[$i][$j])*3.6,5.2 , $data[$i][$j], 1,0,'C');
            }

            $perc = (string)((((int)$ClassPresent[$i])/((int)$sum))*100);
            $perc  = number_format((float) $perc, 1, '.', '');
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(9,5.2 , $ClassPresent[$i], 1,0,'C');
            $this->Cell(9,5.2 ,$perc , 1,0,'C');
            $this->SetFont('Arial', '', 10);
            $this->Ln();
        }

    }
}

include 'db.php';
$Month = array("01"=>"January","02"=>"February","03"=>"March","04"=>"April","05"=>"May","06"=>"June","07"=>"July","08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");

    $SubjectCode = $_POST['Code'];
    $mon = $_POST['Mon'];
    $month = $Month[$_POST['Mon']];

    $query = "SELECT COLUMN_NAME 
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = '$SubjectCode' AND TABLE_SCHEMA='$database'";
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    $j = 0;
    while($row = $result->fetch_assoc())
    {
        ++$j;
        $rowMon = substr($row['COLUMN_NAME'],4,2);
        if($rowMon == $mon)
        {
            break;
        }
    }
    $Dat = array();$rowAttend = array(); $TotCl = array();
    $TDArray = array(array());
    $LenArr = array();
    $la = 0;
    $d = 0;

    $Year = (int)substr($row['COLUMN_NAME'],7,2);

    $n = (int)substr($row['COLUMN_NAME'],1,2);

    $Dat[$d++] = $n;

    $TotCl[0] = (int)substr($row['COLUMN_NAME'],-1);
    $LenArr[0] = 1;

    while($row = $result->fetch_assoc())
    {
        $rowMon = substr($row['COLUMN_NAME'],4,2);
        if($rowMon == $mon)
        {
            $n = (int)substr($row['COLUMN_NAME'],1,2);
            if($Dat[$d-1] == $n)
            {
                $LenArr[$la]++;
                $TotCl[$d-1] += (int)substr($row['COLUMN_NAME'], -1);
            }
            else
            {
                $la++;
                $LenArr[$la] = 1;
                $Dat[$d++] = $n;
                $TotCl[$d - 1] = (int)substr($row['COLUMN_NAME'], -1);
            }
        }
        else
        {
            break;
        }
    }

  //  echo "No ";
  /*  for($i = 0;$i<$d;$i++)
    {
        echo $Dat[$i]."($TotCl[$i]) ->  $LenArr[$i]";
        echo "<br>";
    }*/

    $m = -1;
    $ClassPresent = array();
    $query = "SELECT * FROM $SubjectCode";
    $result = $conn->query($query);
    while($row = $result->fetch_assoc())
    {
        $n = 0;

        $rowAttend = array_values($row);
        $TDArray[++$m][$n++] = $rowAttend[0];
        $ClassPresent[$m] = 0;

        $z = -1;
        $f = $j;
        for($i = 0;$i<$d;$i++)
        {
            $str="";
            $limit = $LenArr[$i];
            $g = 0;
            $num = 0;
            for(;$g<$limit;$f++,$g++) {
                $num += $rowAttend[$f];
                $ClassPresent[$m] += $rowAttend[$f];

            }
           // $j += $limit;

            for($a = 0;$a<$num;$a++)
                $str.="P";

            $tot = $TotCl[$i]-$num;

            for($a = 0;$a<$tot;$a++)
                $str.="A";

            $TDArray[$m][$n++] = $str;//$TotCl[$i]."_".$rowAttend[$f];
        }
        //echo "<br>";
    }
    $m++;
    $sum = array_sum($TotCl);
/*
for($i=0;$i<5;$i++)
{
    for($j=0;$j<=$d;$j++)
        echo $TDArray[$i][$j].' ';
    echo '<br>';
}*/

    $NameStd = array();
    for($i = 0;$i<$m;$i++)
    {
        //echo $TDArray[$i][0]." ";
        $roll = $TDArray[$i][0];
        $query = "SELECT FirstName, MiddleName, LastName FROM StudentBase WHERE RollNo = '$roll'";
        //$query = "SELECT FirstName, MiddleName, LastName FROM StudentBase WHERE RollNo = '$TDArray[$i][0]'";
        $result = $conn->query($query);
        $rowStd = $result->fetch_assoc();

        if($rowStd['MiddleName'] == null)
            $NameStd[$i] = $rowStd['FirstName'].' '.$rowStd['LastName'];
        else
            $NameStd[$i] = $rowStd['FirstName'].' '.$rowStd['MiddleName'].' '.$rowStd['LastName'];
        //echo $NameStd[$i]."<br>";

    }
    $query = "SELECT SubjectName, Semester FROM Subjects WHERE SubjectCode = '$SubjectCode'";
    $result = $conn->query($query);
    $rowSub = $result->fetch_assoc();
    $Sub = $rowSub['SubjectName'];
    $Semester = $rowSub['Semester'];

    $pdf = new PDF();
    // Column headings
    $header = array();
    $header[0] = "SNo";
    $header[1] = "Name";
    for($i = 0;$i<$d;$i++)
        $header[$i+2] = $Dat[$i];

    $pdf->SetFont('Arial','',10);
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->BasicTable($header,$TDArray,$NameStd,$TotCl,$month,$Year,$Sub,$Semester,$sum,$ClassPresent);
    //foreach($ClassPresent as $d)
       // echo $d;
    $pdf->Output($month."_".$Sub."Attendance.pdf",'D');
?>