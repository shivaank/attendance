<?php
require 'db.php';

$JSONstr ='{
                "RollNo":"15BCS0040",
                "SubjectCode":"CEN502"
           }';
$JSONstr = file_get_contents('php://input');

class resp
{
    function resp()
    {
        $this->RollNo = null;
        $this->Name = null;
        $this->Subject = null;
        $this->TeacherName = null;
        $this->Attendance = array();
        $this->ErrorCode = 0;
        $this->Message = null;
    }
}
class Attend
{
    function Attend()
    {
        $this->Month = null;
        $this->Day = array();
        $this->Present = array();
    }
}

$response = new resp();

$myObj = json_decode($JSONstr);
$RollNo = trim($myObj->RollNo);
$SubjectCode = trim($myObj->SubjectCode);

if(strlen($RollNo) <= 2)
{
if(strlen($RollNo) == 1)
        $RollNo = "0".$RollNo; 
    $query = "SELECT RollNo FROM $SubjectCode WHERE  RollNo LIKE '%$RollNo'";
    $res = $conn->query($query);
    if($res) {
        if ($res->num_rows == 0) {
            $response->ErrorCode = 1;
            $response->Message = "Invalid roll number provided.";
            echo json_encode($response);
            exit(0);
        }
        while ($row = $res->fetch_assoc()) {
            $RollNo = $row['RollNo'];
        }
    }
    else
    {
        $response->ErrorCode = 1;
        $response->Message = "Subject cannot be found.";
    }
}

$rowCol = array();

$response->RollNo = $RollNo;

$qName = "SELECT Name FROM UserBase WHERE KeyString = '$RollNo'";
$resName = $conn->query($qName);
$rowName = $resName->fetch_assoc();

$response->Name = $rowName['Name'];


$query = "SELECT SubjectName, TId FROM Subjects WHERE SubjectCode = '$SubjectCode'";
$result = $conn->query($query);
if(mysqli_num_rows($result) != 0)
{
    $row = $result->fetch_assoc();
    //$SubjectCode = $SubjectCode;
    $SubjectName = $row['SubjectName'];
    $response->Subject =  $SubjectName;
    $TId = $row['TId'];
    if($TId != null)
    {
        $qTeacher = "SELECT FirstName, MiddleName, LastName FROM TeacherBase WHERE TId = '$TId'";
        $resTeacher = $conn->query($qTeacher);

        if($resTeacher)
        {
            $rowTName = $resTeacher->fetch_assoc();
            if($rowTName['MiddleName'] == null && $rowTName['LastName'] == null)
                $TeacherName = $rowTName['FirstName'];
            else if($rowTName['MiddleName'] == null)
                $TeacherName = $rowTName['FirstName']." ".$rowTName['LastName'];
            else
                $TeacherName = $rowTName['FirstName']." ".$rowTName['MiddleName']." ".$rowTName['LastName'];
            $response->TeacherName = $TeacherName;
        }
        $qAttend = "SELECT * FROM $SubjectCode WHERE RollNo = '$RollNo'";
        $resAttend = $conn->query($qAttend);
        if(mysqli_num_rows($resAttend) != 0)
        {
            $rowAttend = $resAttend->fetch_assoc();
            $rowAttend = array_values($rowAttend);
            //echo count($rowAttend);

            $qCol = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$SubjectCode' AND TABLE_SCHEMA='$database'";

            $j = -1;

            $Month = array("01"=>"January","02"=>"February","03"=>"March","04"=>"April","05"=>"May","06"=>"June","07"=>"July","08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");

            $resCol = $conn->query($qCol);

            if(mysqli_num_rows($resCol) == 1)
            {
                $response->ErrorCode = 1;
                $response->Message = "Attendance of this subject is not present on the server.";
            }
            else
            {
                while ($row = $resCol->fetch_assoc()) {
                    $rowCol[++$j] = $row['COLUMN_NAME'];
                }

                /* for($i = 0;$i<=$j;$i++)
                 {
                     //echo $rowCol[$i];
                 }
                 //echo "<br>";
                 for($i = 1;$i<count($rowAttend);$i++)
                 {
                 //   echo $rowAttend[$i];
                 }
                 */
                $n = count($rowAttend);

                $TDArray = array(array());
                $q = -1;
                $ctr = 1;
                $mon = substr($rowCol[1], 4, 2);
                $day = substr($rowCol[1], 1, 2);

                $tot = (int)substr($rowCol[1], -1);
                $sum = $rowAttend[1];
                for ($i = 2; $i < $n; $i++) {
                    if (substr($rowCol[$i], 4, 2) == $mon && substr($rowCol[$i], 1, 2) == $day) {
                        $tot += (int)substr($rowCol[$i], -1);
                        $sum += $rowAttend[$i];
                        //$ctr++;
                    } else {
                        $TDArray[0][++$q] = $mon . "_" . $day . "_" . $tot;
                        $TDArray[1][$q] = $sum;
                        $sum = $rowAttend[$i];
                        $tot = (int)substr($rowCol[$i], -1);
                        //echo $ctr;
                        //$ctr = 1;
                        $mon = substr($rowCol[$i], 4, 2);
                        $day = substr($rowCol[$i], 1, 2);
                    }
                }
                //echo $ctr;
                $TDArray[0][++$q] = $mon . "_" . $day . "_" . $tot;
                $TDArray[1][$q] = $sum;
                /*for($i = 0;$i < 2;$i++)
                {
                    for($j = 0;$j<=$q;$j++)
                    {
                        echo $TDArray[$i][$j]." ";
                    }
                    echo "<br>";
                }*/
                $attend = new Attend();
                $mon = substr($TDArray[0][0], 0, 2);
                $attend->Month = $Month[substr($TDArray[0][0], 0, 2)];
                $day = substr($TDArray[0][0], 3);
                if((int)(substr($day,0,2)) < 10)
                            $day = substr($day, 1, 3);
                $attend->Day[0] = $day;
                $attend->Present[0] = (int)$TDArray[1][0];
                $p = 0;
                $l = -1;

                for ($i = 1; $i <= $q; $i++) {
                    if (substr($TDArray[0][$i], 0, 2) == $mon) {
$day = substr($TDArray[0][$i], 3);
                        if((int)(substr($day,0,2)) < 10)
                            $day = substr($day, 1, 3);
                        $attend->Day[++$p] = $day;
                        $attend->Present[$p] = (int)$TDArray[1][$i];
                    } else {
                        $response->Attendance[++$l] = $attend;
                        $p = -1;
                        $attend = new Attend();
                        $mon = substr($TDArray[0][$i], 0, 2);
                        $attend->Month = $Month[substr($TDArray[0][$i], 0, 2)];
                        $day= substr($TDArray[0][$i], 3);
                        if((int)(substr($day,0,2)) < 10)
                            $day = substr($day, 1, 3);
                        $attend->Day[++$p]  = $day;
                        $attend->Present[$p] = (int)$TDArray[1][$i];
                    }
                }
                $response->Attendance[++$l] = $attend;
            }
        }
        else
        {
            $response->ErrorCode = 1;
            $response->Message = "You are not registered for this subject yet.";
        }

    }
    else
    {
        $response->ErrorCode = 1;
        $response->Message = "This subject hasn't been registered yet.";
    }
}
else
{
    $response->ErrorCode = 1;
    $response->Message = "Subject cannot be found.";
}

echo json_encode($response);

?>