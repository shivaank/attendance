<?php
session_start();
include 'db.php';
#echo "Session Started";
if(!isset($_SESSION['status'])){
    $_SESSION['status']=0;
    $status = $_SESSION['status'];
    header('location:index.php');
    exit(0);
}
else{
    $status = $_SESSION['status'];
    if($status == 2){
        header('location:studentDashboard.php');
        echo "$status";
        exit(0);

    }elseif($status == 0){
        header('location:index.php');
        echo "$status";
        exit(0);
    }
    elseif($status == 3){ }
    else
    {
        header('location:index.php');
        echo "$status";
        exit(0);
    }
}

$status =$_SESSION['status'];
$first_name =$_SESSION['first_name'];
$teacher_id = $_SESSION['teacher_id'];
$middle_name = $_SESSION['middle_name'];
$last_name = $_SESSION['last_name'];
$Email = $_SESSION['Email'];
$first_name = $first_name.' '.$middle_name;

echo "
      <!doctype html>
      <html>

      <head>
          <meta charset='UTF-8'>
          <meta name='viewport' content='width=device-width, initial-scale=1'>
          <link rel='stylesheet' href='css/roboto.css' type='text/css'>
          <link href='css/MatIco.css' rel='stylesheet'>
          <link href='css/bootstrap.min.css' rel='stylesheet'>
          <link href='css/bootstrap-material-design.css' rel='stylesheet'>
          <link href='css/ripples.min.css' rel='stylesheet'>
          <link href='css/snackbar.css' rel='stylesheet'>
          <link href='css/responsivetable.css' rel='stylesheet'>          
          <link href='css/modal.css' rel='stylesheet'>
          <title>Medical Leave</title>
          
          <!-- modal css -->
     <!--     <style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */

        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            align-content: center;
            border: 1px solid #888;
            width: 80%;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }
        
        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {top:-300px; opacity:0} 
            to {top:0; opacity:1}
        }
        
        @keyframes animatetop {
            from {top:-300px; opacity:0}
            to {top:0; opacity:1}
        }
        
        /* The Close Button */
        .close {
            color: white;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }
        
        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }
        
        .modal-header {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }
        
        .modal-body {padding: 2px 16px;}
        
        .modal-footer {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }
        </style> 
          modal css over --> 
                 
          <script>
        
        var btn = document.getElementById(\"myBtn\");
               
            function clear_table()
           {
                document.getElementById('tabAttendance').innerHTML = '';
                document.getElementById('msg_backend_register').innerHTML = '';
           }
               
            function subject_acc_to_sem(){
              var sem = document.getElementById('semesterSelect').value;
              
              if (window.XMLHttpRequest) {

                  xmlhttp = new XMLHttpRequest();
              } else {

                  xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
              }
              var res_text;
              xmlhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                      document.getElementById('subjectSelect').innerHTML = this.responseText;
                  }
              };
              xmlhttp.open('GET','teacherBackend.php?req='+1+'&semester='+sem,true);
              xmlhttp.send();
          }
          
          function putStdList()
          {
                var Code = document.getElementById('subjectSelect').value;
                if (window.XMLHttpRequest) {

                  xmlhttp = new XMLHttpRequest();
              } else {

                  xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
              }
                xmlhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                      document.getElementById('RollNo').innerHTML = this.responseText;
                  }
              };
              xmlhttp.open('GET','teacherBackend.php?req='+6+'&SubjectCode='+Code,true);
              xmlhttp.send();
          }
          function putName()
          {
              var Roll = document.getElementById('RollNo').value;
               var span = document.getElementsByClassName(\"close\")[0];
               
              if (window.XMLHttpRequest) {

              xmlhttp = new XMLHttpRequest();
              } else {

                  xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
              }
                xmlhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                      document.getElementById('Name').innerHTML = this.responseText;
                  }
              };
              xmlhttp.open('GET','teacherBackend.php?req='+7+'&RollNo='+Roll,true);
              xmlhttp.send();
          }
          function submitMedical()
          {
               var modal = document.getElementById('myModal');
          
               var Code = document.getElementById('subjectSelect').value;
               var Roll = document.getElementById('RollNo').value;
               var start = document.getElementById('start').value;
               var end = document.getElementById('end').value;
                          
               
               if (window.XMLHttpRequest) {

              xmlhttp = new XMLHttpRequest();
              } else {

                  xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
              }
                xmlhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                      if(this.responseText == '1')
                      {
                            document.getElementById('modal-header').style.backgroundColor = '#5cb85c';
                            document.getElementById('show').innerHTML='<b>Medical leave applied successfully.</b>';
                            document.getElementById('show2').innerHTML='To close this pop-up, please click outside.';
                             modal.style.display = 'block';
                      }
                      else if(this.responseText == '2')
                      {
                            document.getElementById('modal-header').style.backgroundColor = 'red';
                            document.getElementById('show').innerHTML='<b>Please select roll number.</b>';
                            document.getElementById('show2').innerHTML='To close this pop-up, please click outside.';
                             modal.style.display = 'block';
                      }
                      else if(this.responseText == '3')
                      {
                            document.getElementById('modal-header').style.backgroundColor = 'red';
                            document.getElementById('show').innerHTML='<b>Please select subject.</b>';
                            document.getElementById('show2').innerHTML='To close this pop-up, please click outside.';
                             modal.style.display = 'block';
                      }
                      else if(this.responseText == '4')
                      {
                            document.getElementById('modal-header').style.backgroundColor = 'red';
                            document.getElementById('show').innerHTML='<b>There\'s a problem with your selection of dates. Please review them.</b>';
                            document.getElementById('show2').innerHTML='To close this pop-up, please click outside.';
                             modal.style.display = 'block';
                      }
                      else
                      {
                        //nhi hua
                            document.getElementById('modal-header').style.backgroundColor = 'red';
                            document.getElementById('show').innerHTML='Some problem occurred. Please try again later.';
                            document.getElementById('show2').innerHTML='To close this pop-up, please click outside.';
                            modal.style.display = 'block';
                      }
                  }
              };
              xmlhttp.open('GET','teacherBackend.php?req='+8+'&RollNo='+Roll+'&SubjectCode='+Code+'&start='+start+'&end='+end,true);
              xmlhttp.send();
               
              //document.getElementById('Name').innerHTML = '<option value=1>'+Code+Roll+start+end+'</option>';
               // When the user clicks on <span> (x), close the modal
                                   
             window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = \"none\";
                }
             }
              
          }
        
            </script>
              
      </head>

      <body>
      
      <!-- modal javascript-->
      
      
      <!-- Trigger/Open The Modal 
        <button id=\"myBtn\">Open Modal</button> -->
        
        <!-- The Modal -->
        <div id=\"myModal\" class=\"modal\">
        
          <!-- Modal content -->
          <div class=\"modal-content\">
            <div class=\"modal-header\" id='modal-header'>
            
              <h2><center>Medical submission status</center></h2>
              
            </div>
            <div class=\"modal-body\">
             <center><p id='show'></p></center> 
              <center><p id='show2'></p></center> 
            </div>
           
          </div>
        
        </div>
      
      
      <!-- modal javascript over -->
      
          
          <div class='bs-component'>
        <div class='navbar navbar-danger'>
            <div class='container-fluid'>
                <div class='navbar-header'>
                    <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-responsive-collapse'>
                        <span class='icon-bar'></span>
                        <span class='icon-bar'></span>
                        <span class='icon-bar'></span>
                      </button>
                    <a class='navbar-brand' href='index.php'>AM</a>
                </div>
                <div class='navbar-collapse collapse navbar-responsive-collapse'>
                    <ul class='nav navbar-nav navbar-left'>
                        <li  onclick=window.open('teacherDashboard.php','_self'); ><a data-toggle='tab' href='teacherDashboard.php'>Goto Dashboard<span class='badge'></span></a></li>
                        <li class='active'><a data-toggle='tab' href='#AddMedical' onclick='' >Medical leave <span class='badge'></span></a></li>
                     </ul>
                    <ul class='nav navbar-nav navbar-right'>
                        <li class=\"dropdown\">
                          <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">$first_name</a>
                          <ul class=\"dropdown-menu\">
                            <li><a href=\"changePassword.php\">Change Password</a></li>
                            <li class=\"divider\"></li>
                            <li><a href=\"test.php\">Sign Out <span class=\"glyphicon glyphicon-log-out pull-right\"></span></a></li>
                          </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
        
              <!--BODY-->
           
           <div class='tab-content row'>";

// new modal se

echo "<br>
   </div> 
   <div class='tab-content row' align='center'>
<div class='container-fluid'>
        <div class='tab-content' align='center'>
            <div id='Add Medical' class='tab-pane fade in active' style='text-align: center;' align='center'>
                <div class='modal-dialog' align='center'>
                    <div class='modal-content col-lg-12' align='center'>
                        <div class='modal-header' >
                            <h2 class='text-center'>
                                Medical leave for :
                            </h2>
                        </div>

                        <div class='modal-body'>
                            <form class='col-lg-12 center-block' action='' method='POST'>
                                <div class='form-group'>
                                <label for='semesterSelect' class='col-md-4'>Semester :</label>
                                <div class='col-md-4'>
                                          <select id='semesterSelect' class='form-control' onchange='subject_acc_to_sem(); clear_table();'>
                                            <option value=0>Select</option>";

$query_to_get_semesters_taught = "SELECT DISTINCT Semester FROM Subjects WHERE TId = '$teacher_id' ORDER BY Semester";
$result = mysqli_query($conn,$query_to_get_semesters_taught);
while($rows = mysqli_fetch_assoc($result)){
    $semester_value = $rows['Semester'];
    if($semester_value > 8)
        $show = "M.Tech Sem ".(string)($semester_value-8);
    else
        $show = "B.Tech Sem ".(string)($semester_value);

    echo "
                  <option value=$semester_value>$show</option>
              ";
}

$Year =  date("Y");
echo "
                                         </select>
                      </div>
                        </div> <br>
                                <div class='form-group'>
                                <label for='subjectSelect' class='col-md-4'>Subject :</label>
                                <div class='col-md-4'>
                                    <select id='subjectSelect' class='form-control' onchange='putStdList();'>
                              <option value='-1' selected>Subject Name</option>
                              </select>
                                </div>
                            </div>
                            <br>
                                <div class='form-group'>
                                   <a class='btn btn-primary' type='button' id='btn-load-list' data-toggle='tab' href='#StudentData'><h5>Go Ahead</h5></a>
                        
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>";
// new modal se over



//Student Details

echo "<br>
   </div> 
   <div class='tab-content row'>
<div class='container-fluid'>
        <div class='tab-content'>
            <div id='StudentData' class='tab-pane fade' style='text-align: center;'>
                <div class='modal-dialog'>
                    <div class='modal-content col-lg-12'>
                        <div class='modal-header'>
                            <h2 class='text-center'>
                                Student Details
                            </h2>
                        </div>

                        <div class='modal-body'>
                            <form class='col-lg-12 center-block' action='' method='POST'>
                                <div class='form-group'>
                                 <label  class='col-md-4'>Roll No :</label>
                                    <select type='text' id = 'RollNo' class='form-control input-lg' placeholder='Search by RollNo' name='RollNo' onchange='putName();' required>
                                    
                                    </select>
                                </div>
                                <div class='form-group'>
                                <label  class='col-md-4'>Name :</label>
                                    <select type='text' id='Name' class='form-control input-lg' placeholder='Name' name='Name' required>
                                     
                                    </select>
                                </div>
                                <div class='form-group'>
                                   <a class='btn btn-primary' type='button' id='' data-toggle='tab' href='#Date'><h5>Go Ahead</h5></a>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>";

//Dates
echo "<br>
</div>
<div class='container-fluid'>
        <div class='tab-content'>
            <div id='Date' class='tab-pane fade' style='text-align: center;'>
                <div class='modal-dialog'>
                    <div class='modal-content col-lg-12'>
                        <div class='modal-header'>
                            <h2 class='text-center'>
                                Select Dates
                            </h2>
                        </div>

                        <div class='modal-body'>
                            <form class='col-lg-12 center-block' action='' method='POST'>
                                <div class='form-group'> 
                            <label class='col-md-4'> Dates : </label>
                               <div class='col-md-4'>
                                 <input type =date id= 'start' name = 'start' min= '$Year-01-01' max= '$Year-12-31'>  to
                                 <input type =date id= 'end' name = 'end' max= '$Year-12-31' min='$Year-01-01'>  
                                </div>
                            </div><br>
                                <div class='form-group'>
                                   <a class='btn btn-primary' type='button' id='myBtn'  data-toggle='tab' onclick='submitMedical();'><h5>Submit Medical</h5></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

";

echo "  
  
            
        <div class='jumbotron' id ='class_list_uneditable'>
            <div class='container-fluid'>
                <div class='col-md-2'></div>
                <div class='col-md-8 col-sm-10 col-xs-9'>
                    <div class='scrolling'>
                        <div class='inner' >

                       <table >
                              
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>       ";

echo"
        <script src='js/jquery-1.10.2.min.js'></script>
        <script src='js/bootstrap.min.js'></script>
        <script src='js/ripples.min.js'></script>
        <script src='js/material.min.js'></script>
        <script src='js/snackbar.min.js'></script>
        <script src='js/jquery.nouislider.min.js'></script>

    </body>
    <hr>
    <footer class=' bscomponent navbar navbar-fixed-bottom navbar-danger'>

        <div class='container-fluid' style='padding:20px;'>
            <div class='row'>
                <div class='col-md-4' style='text-align:center;'>
                    <a href='#' style='color:#FFF;'>About Us</a>
                </div>
                <div class='col-md-4' style='text-align:center;color:#FFF;'>
                    <a href='#' style='color:#FFF;'>Contact Us</a>
                </div>
                <div class='col-md-4' style='text-align:center;color:#FFF;'>
                    <a href='#' style='color:#FFF;'>Team</a>
                </div>
            </div>
        </div>
    </footer>

    </html>";
?>