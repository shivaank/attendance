<?php
require 'db.php';

$JSONstr ='{
                "SubjectCode":"CEN502"
           }';
$JSONstr = file_get_contents('php://input');

$SubjectCode =  json_decode($JSONstr)->SubjectCode;

class resp
{
    function resp()
    {
        $this->Dates = array();
        $this->Attendance = array();
        $this->Error = "0";
        $this->Message = null;
    }
}
class Date
{
    function Date()
    {
        $this->Month = null;
        $this->Array = array();
    }
}
class Attend
{
    function Attend()
    {
        $this->RollNo = null;
        $this->Name = null;
        $this->Present = array();
    }
}

$response = new resp();

$Month = array("01"=>"January","02"=>"February","03"=>"March","04"=>"April","05"=>"May","06"=>"June","07"=>"July","08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");

$query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$SubjectCode' AND TABLE_SCHEMA='$database'";
$result = $conn->query($query);
if(!$result)
{
    $response->Error = "1";
    $response->Message = "Attendance of this subject is not present on the server.";
    echo json_encode($response);
    exit(0);
}

if(mysqli_num_rows($result) == 1 || mysqli_num_rows($result) == 0)
{
    $response->Error = "1";
    $response->Message = "Attendance of this subject is not present on the server.";
    echo json_encode($response);
    exit(0);
}
$row = $result->fetch_assoc();
$response->Dates = array();
$j = 0;

$dat = new Date();
$row = $result->fetch_assoc();
$mm = substr($row['COLUMN_NAME'],4,2);
$dd = substr($row['COLUMN_NAME'],1,2);
if($dd < 10)
    $dd = substr($dd,-1);
$noc = substr($row['COLUMN_NAME'],-1);
$dat->Month = $Month[$mm];
$dat->Array = array();
$dat->Array[0] = $dd."_".$noc;
$p = 1;

while($row = $result->fetch_assoc())
{
    if($mm == substr($row['COLUMN_NAME'],4,2))
    {
        $dd = substr($row['COLUMN_NAME'],1,2);
        $noc = substr($row['COLUMN_NAME'],-1);
        if($dd < 10)
            $dd = substr($dd,-1);
        $dat->Array[$p++] = $dd."_".$noc;
    }
    else
    {
        $response->Dates[$j++] = $dat;

        $dat = new Date();
        $mm = substr($row['COLUMN_NAME'],4,2);
        $dd = substr($row['COLUMN_NAME'],1,2);
        $noc = substr($row['COLUMN_NAME'],-1);
        if($dd < 10)
            $dd = substr($dd,-1);
        $dat->Month = $Month[$mm];
        $dat->Array = array();
        $dat->Array[0] = $dd."_".$noc;
        $p = 1;
    }
    //echo $row['COLUMN_NAME']."<br>";
}
$response->Dates[$j++] = $dat;

// code for attendance
$response->Attendance = array();
$att = 0;

$query = "SELECT * FROM $SubjectCode";
$result = $conn->query($query);
while($row = $result->fetch_assoc())
{
    // roll no and name
    $attend = new Attend();
    $attend->RollNo = $row['RollNo'];
    $qName = "SELECT Name FROM UserBase WHERE KeyString = '$attend->RollNo'";
    $rName = $conn->query($qName)->fetch_assoc();
    $attend->Name  = $rName['Name'];

    // present array

    $attend->Present = array();
    $pres = 0;

    $n = count($response->Dates);
    $last = 1;
    for($i = 0;$i<$n;$i++)
    {
        $dat = new Date();
        $dat->Month = $response->Dates[$i]->Month;
        $dat->Array = array();
        $q = 0;
        $noOfClass = count($response->Dates[$i]->Array);
        $row = array_values($row);
        for($ctr = $last;$ctr < $last+$noOfClass;$ctr++)
        {
            $dat->Array[$q++] = $row[$ctr];
        }
        $last = $ctr;
        $attend->Present[$pres++] = $dat;
    }
    $response->Attendance[$att++] = $attend;

}


echo json_encode($response);

?>