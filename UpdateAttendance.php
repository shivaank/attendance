<?php

require 'db.php';
$JSONstr = file_get_contents('php://input');
/*
$JSONstr ='{

                  "Date":"_31_08_17_11",

                  "NoOfClass":"2",

		          "VerifyStr":"JMIAM0004",

                  "SubjectCode":"CEN502",

                  "Attendance": ["15BCS0003","15BCS0004","15BCS0006","15BCS0007","15BCS0008","15BCS0009","15BCS0010","15BCS0013","15BCS0014","15BCS0015","15BCS0016","15BCS0018","15BCS0019","15BCS0020","15BCS0021","15BCS0022","15BCS0027","15BCS0029","15BCS0030","15BCS0031","15BCS0034","15BCS0035","15BCS0037","15BCS0038","15BCS0039","15BCS0040","15BCS0041","15BCS0042","15BCS0045","15BCS0046","15BCS0051","15BCS0052","15BCS0053","15BCS0056","15BCS0059","15BCS0062","15BCS0065","15BCS0063","15BCS0064","15BCS0067","15BCS0068","15BCS0069","15BCS0070","15BCS0073","15BCS0074","15BCS0075","15BCS0081"]

               

           }';*/

class resp 
    {
        function resp()
        {
            $this->ErrorCode = "0";
            $this->Message = null;
        }
    }
$response = new resp();
    
$MyObj = json_decode($JSONstr);
$TId = "";
$Email = "";
$phoneNo = "";

$VerifyStr = trim($MyObj->VerifyStr);
//$Password = md5(md5($MyObj->Password));
$SubjectCode = trim($MyObj->SubjectCode);

$flag = 0;

if(substr($VerifyStr,0, 5) == "JMIAM" && strlen($VerifyStr) == 9)
{
    //echo "TId";
    $TId = $VerifyStr;
    $query  = "SELECT Email FROM TeacherBase WHERE TId = '$VerifyStr'";
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    $Email =  $row['Email'];
    
    $query  = "SELECT Status FROM UserBase WHERE Email = '$Email'";
    $result = $conn->query($query);
    if(@$row = $result->fetch_assoc())
    {
        
            if($row['Status'] == 3)
            {
                //match subcode and TId
                $query  = "SELECT TId FROM Subjects WHERE SubjectCode = '$SubjectCode'";
                $result = $conn->query($query);
                $row = $result->fetch_assoc();
                if($row['TId'] == $TId)
                    $flag = 5;
                else
                    $flag = 3;
            }
            else
                $flag = 2;
        
    }
    
}
else if(filter_var($VerifyStr, FILTER_VALIDATE_EMAIL))
{
    //echo "Email";
    
    $query  = "SELECT TId FROM TeacherBase WHERE Email = '$VerifyStr'";
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    $TId =  $row['TId'];
    
    $Email = $VerifyStr;
    $query  = "SELECT Email, Status FROM UserBase WHERE Email = '$Email'";
    $result = $conn->query($query);
    if(@$row = $result->fetch_assoc())
    {
        
            if($row['Status'] == 3)
            {
                //match subcode and TId
                $query  = "SELECT TId FROM Subjects WHERE SubjectCode = '$SubjectCode'";
                $result = $conn->query($query);
                $row = $result->fetch_assoc();
                if($row['TId'] == $TId)
                {
                    $flag = 5;
                }
                else
                    $flag = 3;
            }
            else
                $flag = 2;
        
    }
}
else if(1 === preg_match('~[0-9]~', $VerifyStr) && strlen($VerifyStr) == 10)
{
    //echo "phone number";
    
    $query  = "SELECT Email FROM UserBase WHERE PhoneNo = '$VerifyStr'";
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    $Email =  $row['Email'];
    
    $query  = "SELECT TId FROM TeacherBase WHERE Email = '$Email'";
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    $TId =  $row['TId'];
    
    $phoneNo = $VerifyStr;    
    $query  = "SELECT Status FROM UserBase WHERE PhoneNo = '$phoneNo'";
    $result = $conn->query($query);
    if(@$row = $result->fetch_assoc())
    {
        if($row['Status'] == 3)
        {
            //match subcode and TId
            $query  = "SELECT TId FROM Subjects WHERE SubjectCode = '$SubjectCode'";
            $result = $conn->query($query);
            $row = $result->fetch_assoc();
            if($row['TId'] == $TId)
            {
                $flag = 5;
            }
            else
                $flag = 3;
        }
        else
            $flag = 2;
        
    }
}

//echo $flag."<br>";
if($flag == 5)
{
    // everything ok. update the attendance
    $Attendance = $MyObj->Attendance;
    $ManualAttendance = $MyObj->ManualAttendance;
    $Date = $MyObj->Date;
    $Date = $Date."_".$MyObj->NoOfClass;
    $NoOfClass = (int)($MyObj->NoOfClass);

    //Check if its a lab
    $lab = 0;
    $query = "SELECT Lab FROM Subjects WHERE SubjectCode = '$SubjectCode'";
    if($result = $conn->query($query))
    {
        $row = $result->fetch_assoc();
        if($row['Lab'] == 1)
        {
            $lab = 1;
        }
    }

    // Merge Attendance

    $lateComers = 1;
    $Column = "";
    $query = "SELECT COLUMN_NAME 
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = '$SubjectCode' AND TABLE_SCHEMA='$database'";
    $result = $conn->query($query);
    while($row = $result->fetch_assoc()){
        $Column = $row['COLUMN_NAME'];
    }
    if(substr($Column,0,9) == substr($Date,0,9))
    {
        if((int)substr($Column,10,2)+(int)substr($Column,-1) > (int)substr($Date,10,2) || $lab == 1)
        {
            // late comers
            $n = (int)substr($Column,-1);
            if($n < $NoOfClass)
                $NoOfClass = $n;

            foreach ($Attendance as $a) {
                $a = trim($a);
                //echo $a."<br>";
                $query = "UPDATE $SubjectCode SET $Column = $NoOfClass WHERE RollNo = '$a'";
                if (!$conn->query($query)) {
                    $flag = 6;
                }
            }

            foreach ($ManualAttendance as $a) {
                $a = (string)($a);
                $len = strlen($a);
                if ($len == 1) {
                    $a = "0" . $a;
                }

                //echo $a."<br>";
                $query = "UPDATE $SubjectCode SET $Column = $NoOfClass WHERE RollNo LIKE '%$a'";
                if (!$conn->query($query)) {
                    $flag = 6;
                }
            }

            if ($flag == 5) {
                $response->ErrorCode = "0";
                $response->Message = "Attendance merged. Thank you.";
            }

        }
        else
        {
            //take fresh attendance
            $lateComers = 0;
        }
    }
    else
    {
        //take fresh attendance
        $lateComers = 0;
    }

    // Fresh attendance

    if($lateComers == 0)
    {
        $query = "ALTER TABLE $SubjectCode ADD COLUMN $Date int DEFAULT 0";
        if ($conn->query($query)) {
            foreach ($Attendance as $a) {
                $a = trim($a);
                //echo $a."<br>";
                $query = "UPDATE $SubjectCode SET $Date = $NoOfClass WHERE RollNo = '$a'";
                if (!$conn->query($query)) {
                    $flag = 6;
                }
            }

            foreach ($ManualAttendance as $a) {
                $a = (string)($a);
                $len = strlen($a);
                if ($len == 1) {
                    $a = "0" . $a;
                }

                //echo $a."<br>";
                $query = "UPDATE $SubjectCode SET $Date = $NoOfClass WHERE RollNo LIKE '%$a'";
                if (!$conn->query($query)) {
                    $flag = 6;
                }
            }
        } else
            $flag = 7;
        if ($flag == 5) {
            $response->ErrorCode = "0";
            $response->Message = "Attendance taken. Thank you.";
        }
    }
    
}
else if($flag == 1)
{
    //password does not match
    $response->ErrorCode = "1";
    $response->Message = "Attendance NOT Taken. Password does not match.";
}
else if($flag == 2)
{
    // pass matches but not a teacher
    $response->ErrorCode = "1";
    $response->Message = "Attendance NOT Taken. Turns out you are not a teacher.";
}
else if($flag == 3)
{
    // pass matches, a teacher, but not not the one teaching that subject
    $response->ErrorCode = "1";
    $response->Message = "Attendance NOT Taken. Seems like you do not teach this subject.";
}
if($flag == 7)
{
    // problem while updating the attendance
    $response->ErrorCode = "2";
    $response->Message = "Attendance NOT Taken. Some problem occurred at the server end. Please try again. Sorry for the inconvenience.";
}
else if($flag == 6)
{
    // column of date not made
    $response->ErrorCode = "1";
    $response->Message = "Attendance NOT Taken. Please take the attendance again. Sorry for the inconvenience.";
}
//echo $flag;
echo json_encode($response);
?>
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
<script>
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyCLd28O-wFDc-uv9Fu02bmKVbJSYp-xZcw",
        authDomain: "jmizone-e8f77.firebaseapp.com",
        databaseURL: "https://jmizone-e8f77.firebaseio.com",
        projectId: "jmizone-e8f77",
        storageBucket: "",
        messagingSenderId: "760487238037"
    };
    firebase.initializeApp(config);
</script>
<script src="backup.js"></script>