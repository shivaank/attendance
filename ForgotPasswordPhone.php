<?php
require 'db.php';

$JSONstr = file_get_contents('php://input');
$Object = json_decode($JSONstr);
$VerifyStr =  $Object->VerifyStr;

class resp
{
    function resp()
    {
        $this->Error = "0";
        $this->Message = null;
    }
}
$response = new resp();
$flag = 0;
if(substr(strtoupper($VerifyStr),0, 5) == "JMIAM" && strlen($VerifyStr) == 9)
{
    //echo "TId";
    $VerifyStr = strtoupper($VerifyStr);
    $query  = "SELECT Email FROM TeacherBase WHERE TId = '$VerifyStr'";
    $result = $conn->query($query);
    if(mysqli_num_rows($result) == 0)
        $flag = 3;
}
else if(filter_var($VerifyStr, FILTER_VALIDATE_EMAIL))
{
    //echo "Email";
    $query  = "SELECT KeyString FROM UserBase WHERE Email = '$VerifyStr'";
    $result = $conn->query($query);
    if(mysqli_num_rows($result) != 0)
    {
        $row = $result->fetch_assoc();
        $VerifyStr =  $row['KeyString'];
    }
    else
        $flag = 3;

}
else if(1 === preg_match('~[0-9]~', $VerifyStr) && strlen($VerifyStr) == 10)
{
    //echo "phone number";

    $query  = "SELECT KeyString FROM UserBase WHERE PhoneNo = '$VerifyStr'";
    $result = $conn->query($query);
    if(mysqli_num_rows($result) != 0)
    {
        $row = $result->fetch_assoc();
        $VerifyStr =  $row['KeyString'];
    }
    else
        $flag = 3;
}
else
{
    $VerifyStr = strtoupper($VerifyStr);
    $query  = "SELECT KeyString FROM UserBase WHERE KeyString = '$VerifyStr'";
    $result = $conn->query($query);
    if(mysqli_num_rows($result) != 0)
    {
        $row = $result->fetch_assoc();
        $VerifyStr =  $row['KeyString'];
    }
    else
        $flag = 3;
}

if($flag == 3)
{
    $response->Error = "1";
    $response->Message = "Invalid Credentials";
    echo json_encode($response);
    exit(0);
}

$query = "SELECT Email, PhoneNo, Name FROM UserBase WHERE KeyString = '$VerifyStr'";
$result = $conn->query($query);
if($result && $JSONstr != "")
{
    $row = $result->fetch_assoc();
    $Email = $row['Email'];
    $PhoneNo = $row['PhoneNo'];
    $Name = $row['Name'];

    $encrypt = substr(md5(rand()),0,6);
    // send message and email

    //message
    $apikey = "33SwQCWTE0irmMdz1lga1A";
    $apisender = "TESTIN";
    $msg ="Hello, $Name.\nWe have successfully changed your password.\nYour new Password is : $encrypt.\nNow that you know your password, we recommend you to change it for security reasons.\nThank you for using Attendance Manager.";
    $num = $PhoneNo;    // MULTIPLE NUMBER VARIABLE PUT HERE...!

    $ms = rawurlencode($msg);   //This for encode your message content

    $url = 'https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey='.$apikey.'&senderid='.$apisender.'&channel=2&DCS=0&flashsms=0&number='.$num.'&text='.$ms.'&route=1';

    //echo $url;
    $ch=curl_init($url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_POSTFIELDS,"");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,2);
    $data = curl_exec($ch);
    //print($data);

    //message sent

    //email
    $to=$Email;
    $subject="Change Password";
    $from = 'Attendance Manager';
    $body="Hello, $Name.\n\nWe have successfully changed your password.\n\nYour new Password is : ".$encrypt."\n\nNow that you know your password, we recommend you to change it for security reasons.\n\nTeam Attendance Manager.";

    //change in database
    $pass=md5(md5($encrypt));
    $sql = "UPDATE UserBase SET Password='$pass' WHERE Email = '$Email'";
    mysqli_query($conn,$sql);
    mail($to,$subject,$body);


    //email sent

    $response->Error = "0";
    $response->Message = "Password successfully changed. You will receive your new password via text message and E-mail shortly.";
}
else
{
    $response->Error = "1";
    $response->Message = "Internal Server Error. Please try again later";

}
echo json_encode($response);
?>