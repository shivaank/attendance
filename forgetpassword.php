<?php
echo "
<!doctype html>
<html>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' href='css/roboto.css' type='text/css'>
    <link href='css/MatIco.css' rel='stylesheet'>
    <link href='css/bootstrap.min.css' rel='stylesheet'>
    <link href='css/bootstrap-material-design.css' rel='stylesheet'>
    <link href='css/ripples.min.css' rel='stylesheet'>
    <link href='css/snackbar.css' rel='stylesheet'>

    <title>Login</title>

</head>
<body>

    <div class='bs-component'>
        <div class='navbar navbar-danger'>
            <div class='container-fluid'>
                <div class='navbar-header'>
                    <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-responsive-collapse'>
                  <span class='icon-bar'></span>
                  <span class='icon-bar'></span>
                  <span class='icon-bar'></span>
                </button>
                    <a class='navbar-brand' href='index.php'>CM</a>
                </div>
                <div class='navbar-collapse collapse navbar-responsive-collapse'>
                    <ul class='nav navbar-nav navbar-right'>
                        <li><a href='signUPForm.php'>Sign Up <span class='glyphicon glyphicon-ok'></span><div class='ripple-container'></div></a></li>
                        <li><a href='#'>LogIn <span class='glyphicon glyphicon-log-in'></span><div class='ripple-container'></div></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    
    


    <script src='js/jquery-1.10.2.min.js'></script>
    <script src='js/bootstrap.min.js'></script>
    <script src='js/ripples.min.js'></script>
    <script src='js/material.min.js'></script>
    <script src='js/snackbar.min.js'></script>
    <script src='js/jquery.nouislider.min.js'></script>

</body>

<hr>
<footer class=' bscomponent navbar navbar-fixed-bottom navbar-danger'>

    <div class='container-fluid' style='padding:20px;'>
        <div class='row'>
            <div class='col-md-4' style='text-align:center;'>
                <a href='#' style='color:#FFF;'>About Us</a>
            </div>
            <div class='col-md-4' style='text-align:center;color:#FFF;'>
                <a href='#' style='color:#FFF;'>Contact Us</a>
            </div>
            <div class='col-md-4' style='text-align:center;color:#FFF;'>
                <a href='#' style='color:#FFF;'>Team</a>
            </div>
        </div>
    </div>
</footer>

</html>";
    include('db.php');
    $email      = mysqli_real_escape_string($conn,$_POST['email_id']);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) // Validate email address
    {
        $message =  "Invalid email address please type a valid email!!";
    }
    else
    {
        $query = "SELECT PhoneNo FROM UserBase where email='".$email."'";
        $result = mysqli_query($conn,$query);
        $Results = mysqli_fetch_array($result);

        if(count($Results)>=1)
        {
            $encrypt = substr(md5(rand()),0,6);
            $message = "Your new password is send to your registered e-mail address.";
            $to=$email;
            $subject="Change Password";
            $from = 'Attendance Manager';
            $body="Hi,\n\nYour Registered Phone No. is ".$Results['PhoneNo']."\n\nYour new Password is ".$encrypt."\n\nIt is recommended to change your password using your dashboard.\n\n--\nAttendance Manager";
            $pass=md5(md5($encrypt));
			$sql = "UPDATE UserBase SET Password='$pass' WHERE Email = '$email'";
            mysqli_query($conn,$sql);
			mail($to,$subject,$body);
        }
        else
        {
            $message = "Account not found please signup now!!";
        }
    }
	echo "<b>$message<b>";
?>
