<?php
    session_start();
    if(!isset($_SESSION['status'])){
        $_SESSION['status']=0;
    }
    else{
        $status = $_SESSION['status'];
        if($status == 3){
            header('location:teacherDashboard.php');
            exit(0);

        }elseif($status == 2){
            header('location:studentDashboard.php');
            exit(0);
        }
    }

echo "
<!doctype html>
<html>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' href='css/roboto.css' type='text/css'>
    <link href='css/MatIco.css' rel='stylesheet'>
    <link href='css/bootstrap.min.css' rel='stylesheet'>
    <link href='css/bootstrap-material-design.css' rel='stylesheet'>
    <link href='css/ripples.min.css' rel='stylesheet'>
    <link href='css/snackbar.css' rel='stylesheet'>
    
    
    <!-- icon  -->
    <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"images/web-icon/apple-touch-icon.png\">
<link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"images/web-icon/favicon-32x32.png\">
<link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"images/web-icon/favicon-16x16.png\">
<link rel=\"manifest\" href=\"images/web-icon/manifest.json\">
<meta name=\"theme-color\" content=\"#ffffff\">
    
    
    <title>Log In</title>

</head>
<!--
<body>

    <div class='bs-component'>
        <div class='navbar navbar-danger'>
            <div class='container-fluid'>
                <div class='navbar-header'>
                    <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-responsive-collapse'>
                  <span class='icon-bar'></span>
                  <span class='icon-bar'></span>
                  <span class='icon-bar'></span>
                </button>
                    <a class='navbar-brand' href='index.php'>CM</a>
                </div>
                <div class='navbar-collapse collapse navbar-responsive-collapse'>
                    <ul class='nav navbar-nav navbar-right'>
                        
                        <li><a href='#'>LogIn <span class='glyphicon glyphicon-log-in'></span><div class='ripple-container'></div></a></li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class='modal-dialog'>
        <div class='modal-content col-lg-12'>
            <div class='modal-header'>
                <h1 class='text-center'>
                    LOGIN
                </h1>
            </div>

            <div class='modal-body'>
                <form class='col-lg-12 center-block' action='loginV.php' method='POST'>
                    <div class='form-group'>
                        <input type='text' class='form-control input-lg' placeholder='Email ID' name='email_id' required>
                    </div>
                    <div class='form-group'>
                        <input type='password' class='form-control input-lg' placeholder='Password' name='password' required>
                    </div>
                    <div class='form-group'>
                        <input type='submit' class='btn btn-block btn-lg btn-primary' name='lgbtn' value='Log In'>
                        <span class='pull-left' style='margin-bottom: 10px;'><a href='login.php'><h5>Sign Up</h5></a></span>
                        <span class='pull-right'><a data-toggle='tab' href='#forgotPassword'><h5>Forgot Password?</h5></a></span>                       
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br>
    
    <div class='container-fluid'>
        <div class='tab-content'>
            <div id='forgotPassword' class='tab-pane fade' style='text-align: center;'>
                <div class='modal-dialog'>
                    <div class='modal-content col-lg-12'>
                        <div class='modal-header'>
                            <h2 class='text-center'>
                                FORGOT PASSWORD?
                            </h2>
                        </div>

                        <div class='modal-body'>
                            <form class='col-lg-12 center-block' action='FORGOT_PASSWORD_REDIRECT_HERE' method='POST'>
                                <div class='form-group'>
                                    <input type='text' class='form-control input-lg' placeholder='Email ID' name='email_id' required>
                                </div>
                                <div class='form-group'>
                                    <input type='submit' class='btn btn-block btn-lg btn-primary' name='lgbtn' value='Send Me A Confirmation E-Mail'>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>


    <script src='js/jquery-1.10.2.min.js'></script>
    <script src='js/bootstrap.min.js'></script>
    <script src='js/ripples.min.js'></script>
    <script src='js/material.min.js'></script>
    <script src='js/snackbar.min.js'></script>
    <script src='js/jquery.nouislider.min.js'></script>

</body> -->

<body>

    <div class='bs-component'>
        <div class='navbar navbar-danger'>
            <div class='container-fluid'>
                <div class='navbar-header'>
                    <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-responsive-collapse'>
                  <span class='icon-bar'></span>
                  <span class='icon-bar'></span>
                  <span class='icon-bar'></span>
                </button>
                    <a class='navbar-brand' href='index.php'>AM</a>
                </div>
                <div class='navbar-collapse collapse navbar-responsive-collapse'>
                    <ul class='nav navbar-nav navbar-right'>
                       
                        <li><a href='#'>LogIn <span class='glyphicon glyphicon-log-in'></span><div class='ripple-container'></div></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class='container-fluid'>
        <div class='modal-dialog'>
            <div class='modal-content col-lg-12'>
                <div class='modal-header'>
                    <h1 class='text-center'>
                        LOGIN
                    </h1>
                </div>

                <div class='modal-body'>
                    <form class='col-lg-12 center-block' action='loginV.php' method='POST'>
                        <div class='form-group'>
                            <input type='text' class='form-control input-lg' placeholder='Email ID' name='email_id' required>
                        </div>
                        <div class='form-group'>
                            <input type='password' class='form-control input-lg' placeholder='Password' name='password' required>
                        </div>
                        <div class='form-group'>
                            <input type='submit' class='btn btn-block btn-lg btn-primary' name='lgbtn' value='Log In'>
                            <span class='pull-left' style='margin-bottom: 10px;'><a href='login.php'><h5>Sign Up</h5></a></span>
                            <span class='pull-right'><a data-toggle='tab' href='#forgotPassword'><h5>Forgot Password?</h5></a></span>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class='container-fluid'>
        <div class='tab-content'>
            <div id='forgotPassword' class='tab-pane fade' style='text-align: center;'>
                <div class='modal-dialog'>
                    <div class='modal-content col-lg-12'>
                        <div class='modal-header'>
                            <h2 class='text-center'>
                                FORGOT PASSWORD?
                            </h2>
                        </div>

                        <div class='modal-body'>
                            <form class='col-lg-12 center-block' action='forgetpassword.php' method='POST'>
                                <div class='form-group'>
                                    <input type='text' class='form-control input-lg' placeholder='Email ID' name='email_id' required>
                                </div>
                                <div class='form-group'>
                                    <input type='submit' class='btn btn-block btn-lg btn-primary' name='lgbtn' value='Send Me A Confirmation E-Mail'>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>";
    echo "  
  
            
        <div class='jumbotron' id ='class_list_uneditable'>
            <div class='container-fluid'>
                <div class='col-md-2'></div>
                <div class='col-md-8 col-sm-10 col-xs-9'>
                    <div class='scrolling'>
                        <div class='inner' >

                       <table >
                              
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>       ";

echo "
    <script src='js/jquery-1.10.2.min.js'></script>
    <script src='js/bootstrap.min.js'></script>
    <script src='js/ripples.min.js'></script>
    <script src='js/material.min.js'></script>
    <script src='js/snackbar.min.js'></script>
    <script src='js/jquery.nouislider.min.js'></script>";


//$city = $details->city;


echo "
<script src='https://www.gstatic.com/firebasejs/4.2.0/firebase.js'></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: 'AIzaSyCLd28O-wFDc-uv9Fu02bmKVbJSYp-xZcw',
    authDomain: 'jmizone-e8f77.firebaseapp.com',
    databaseURL: 'https://jmizone-e8f77.firebaseio.com',
    projectId: 'jmizone-e8f77',
    storageBucket: 'jmizone-e8f77.appspot.com',
    messagingSenderId: '760487238037'
  };
  firebase.initializeApp(config);

var firebaseRef = firebase.database().ref();
function detectmob() {
    if( navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
    ){
        return true;
    }
    else {
        return false;
    }
}

if(detectmob() == true)
    firebaseRef.child(Date()+'LoginPHPlocalhost').set('phone');
else
    firebaseRef.child(Date()+'LoginPHPlocalhost').set('PC');
</script>

 

</body>

<hr>
<footer class=' bscomponent navbar navbar-fixed-bottom navbar-danger'>

    <div class='container-fluid' style='padding:20px;'>
        <div class='row'>
            <div class='col-md-4' style='text-align:center;'>
                <a href='#' style='color:#FFF;'>About Us</a>
            </div>
            <div class='col-md-4' style='text-align:center;color:#FFF;'>
                <a href='#' style='color:#FFF;'>Contact Us</a>
            </div>
            <div class='col-md-4' style='text-align:center;color:#FFF;'>
                <a href='#' style='color:#FFF;'>Team</a>
            </div>
        </div>
    </div>
</footer>

</html>";

?>
