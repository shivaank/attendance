<?php
    session_start();
    include 'db.php';
    #echo "Session Started";
    if(!isset($_SESSION['status'])){
        $_SESSION['status']=0;
        $status = $_SESSION['status'];
        header('location:index.php');
        exit(0);
    }
    else{
        $status = $_SESSION['status'];
        if($status == 2){
            header('location:studentDashboard.php');
            echo "$status";
            exit(0);

        }elseif($status == 0){
            header('location:index.php');
            echo "$status";
            exit(0);
        }
        elseif($status == 3){ }
        else
        {
            header('location:index.php');
            echo "$status";
            exit(0);
        }
    }

        $status =$_SESSION['status'];
        $first_name =$_SESSION['first_name'];
        $teacher_id = $_SESSION['teacher_id'];
        $middle_name = $_SESSION['middle_name'];
        $last_name = $_SESSION['last_name'];
        $Email = $_SESSION['Email'];
        $first_name = $first_name.' '.$middle_name.' '.$last_name;

      echo "
      <!doctype html>
      <html>

      <head>
            <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>

          <meta charset='UTF-8'>
          <meta name='viewport' content='width=device-width, initial-scale=1'>
          <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

          <link rel='stylesheet' href='css/roboto.css' type='text/css'>
          <link href='css/MatIco.css' rel='stylesheet'>
          <link href='css/bootstrap.min.css' rel='stylesheet'>
          <link href='css/bootstrap-material-design.css' rel='stylesheet'>
          <link href='css/ripples.min.css' rel='stylesheet'>
          <link href='css/snackbar.css' rel='stylesheet'>
          <link href='css/responsivetable.css' rel='stylesheet'>
          
          <link href='css/modal.css' rel='stylesheet'>
          <link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">
          <title>Teacher Dashboard</title>
          <script>
          
              function clear_table()
               {
                    var id = 'modal'+arguments[0];
                    document.getElementById('modal1').style.display='none';
                    document.getElementById('modal2').style.display='none';
                    document.getElementById('modal4').style.display='none';    // add new attendance
                    document.getElementById('modal3').style.display='none';
                    document.getElementById('modal5').style.display='none';
                    document.getElementById('tabAttendance').innerHTML = '';
                    document.getElementById('msg_backend_register').innerHTML = '';
                    
                    document.getElementById(id).style.display='block';
               }
          
              function subject_acc_to_sem(){
                  var sem = document.getElementById('semesterSelect').value;
                  
                  if (window.XMLHttpRequest) {

                      xmlhttp = new XMLHttpRequest();
                  } else {

                      xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
                  }
                  var res_text;
                  xmlhttp.onreadystatechange = function() {
                      if (this.readyState == 4 && this.status == 200) {
                          document.getElementById('subjectSelect').innerHTML = this.responseText;
                      }
                  };
                  xmlhttp.open('GET','teacherBackend.php?req='+1+'&semester='+sem,true);
                  xmlhttp.send();
              }
              function subject_acc_to_sem_edit(){
                  var sem = document.getElementById('semesterSelectEdit').value;
                  
                  if (window.XMLHttpRequest) {

                      xmlhttp = new XMLHttpRequest();
                  } else {

                      xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
                  }
                  var res_text;
                  xmlhttp.onreadystatechange = function() {
                      if (this.readyState == 4 && this.status == 200) {
                          document.getElementById('subjectSelectEdit').innerHTML = this.responseText;
                      }
                  };
                  xmlhttp.open('GET','teacherBackend.php?req='+1+'&semester='+sem,true);
                  xmlhttp.send();
              }             
              function subject_acc_to_sem_add_att(){
                  var sem = document.getElementById('semesterSelectAddAtt').value;
                  
                  if (window.XMLHttpRequest) {

                      xmlhttp = new XMLHttpRequest();
                  } else {

                      xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
                  }
                  var res_text;
                  xmlhttp.onreadystatechange = function() {
                      if (this.readyState == 4 && this.status == 200) {
                          document.getElementById('subjectSelectAddAtt').innerHTML = this.responseText;
                      }
                  };
                  xmlhttp.open('GET','teacherBackend.php?req='+1+'&semester='+sem,true);
                  xmlhttp.send();
              }      
              
              
              function load_complete_list(){
                    var sem = document.getElementById('semesterSelect').value;
                    var SubjectCode = document.getElementById('subjectSelect').value;
                    //var tabData = '<tr><th>Date </th><td>Content One</td><td>Longer Content Two</td><td>Third Content Contains More</td>                                    <td>Short Four</td><td>Standard Five</td>                                    <td>Who\'s Counting</td></tr> <tr><th>lolo</th><td>12</td><td>12</td><td>12</td><td>12</td><td>12</td><td>12</td></tr><tr><th>lolo</th><td>12</td><td>12</td><td>12</td><td>12</td><td>12</td><td>12</td></tr><tr><th>lolo</th><td>12</td><td>12</td><td>12</td><td>12</td><td>12</td><td>12</td></tr>';
                    
                  if (window.XMLHttpRequest) {

                       xmlhttp2 = new XMLHttpRequest();
                  } else {
                  
                      xmlhttp2 = new ActiveXObject('Microsoft.XMLHTTP');
                  }
                   xmlhttp2.onreadystatechange = function() {
                      if (this.readyState == 4 && this.status == 200) {
                            document.getElementById('tabAttendance').innerHTML = this.responseText;
                      }
                  };
                  xmlhttp2.open('GET','teacherBackend.php?req='+4+'&semester='+sem+'&SubjectCode='+SubjectCode,true);
                  xmlhttp2.send();    
              }
              
           
              function add_subject_acc_to_sem()
              {
                    var sem = document.getElementById('addSemesterSelect').value;
                  if (window.XMLHttpRequest) {
                       xmlhttp2 = new XMLHttpRequest();
                  } else {

                      xmlhttp2 = new ActiveXObject('Microsoft.XMLHTTP');
                  }
                   xmlhttp2.onreadystatechange = function() {
                      if (this.readyState == 4 && this.status == 200) {
                       document.getElementById('addSubjectSelect').innerHTML = this.responseText;
                      }
                  };
                  xmlhttp2.open('GET','teacherBackend.php?req='+2+'&semester='+sem,true);
                  xmlhttp2.send();   
              }
              function registerSubject(){
                    var sem = document.getElementById('addSemesterSelect').value;
                    var SubjectCode = document.getElementById('addSubjectSelect').value;
                    
                    if (window.XMLHttpRequest) {

                       xmlhttp2 = new XMLHttpRequest();
                  } else {

                      xmlhttp2 = new ActiveXObject('Microsoft.XMLHTTP');
                  }
                   xmlhttp2.onreadystatechange = function() {
                      if (this.readyState == 4 && this.status == 200) {
                       document.getElementById('msg_backend_register').innerHTML = this.responseText;
                      }
                  };
                  xmlhttp2.open('GET','teacherBackend.php?req='+3+'&semester='+sem+'&SubjectCode='+SubjectCode,true);
                  xmlhttp2.send();
              }

              function attendance_of_month(){
                 /* var subject = document.getElementById('subjectSelect').value;
                  var sem = document.getElementById('semesterSelect').value;
                  var month = arguments[0];
                  var win = window.open('showAttendanceOfMonth.php?month='+month+'&subject='+subject+'&semester='+sem, '_blank');*/
                               
                    // Create a form
                    var mapForm = document.createElement('form');
                    mapForm.target = '_blank';    
                    mapForm.method = 'POST';
                    mapForm.action = 'showAttendanceOfMonth.php';
                    
                    // Create an input
                    var subject = document.createElement('input');
                    subject.type = 'text';
                    subject.name = 'subject';
                    subject.value = document.getElementById('subjectSelect').value;
                    mapForm.appendChild(subject);
                    
                    var sem = document.createElement('input');
                    sem.type = 'text';
                    sem.name = 'semester';
                    sem.value = document.getElementById('semesterSelect').value;
                    mapForm.appendChild(sem);
                    
                    var month = document.createElement('input');
                    month.type = 'text';
                    month.name = 'month';
                    month.value = arguments[0];
                    
                    // Add the input to the form
                    mapForm.appendChild(month);
                    
                    // Add the form to dom
                    document.body.appendChild(mapForm);
                    
                    // Just submit
                    mapForm.submit();                 
                            
                               
              }
              function openEditing(){
                  var subject = document.getElementById('subjectSelectEdit').value;
                  var sem = document.getElementById('semesterSelectEdit').value;
                  var month = document.getElementById('monthSelectEdit').value;
                  var win = window.open('EditAttendance.php?month='+month+'&subject='+subject+'&semester='+sem, '_blank');
              }
              
              function putRollList() {
               
                    var Code = document.getElementById('subjectSelectAddAtt').value;
                   if (window.XMLHttpRequest) {
    
                      xmlhttp = new XMLHttpRequest();
                  } else {
    
                      xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
                  }
                    xmlhttp.onreadystatechange = function() {
                      if (this.readyState == 4 && this.status == 200) {
                          document.getElementById('RollList').innerHTML = this.responseText;
                      }
                  };
                  xmlhttp.open('GET','teacherBackend.php?req='+9+'&SubjectCode='+Code,true);
                  xmlhttp.send();
                  
              }
              
              var present = {};
              
              function attButClick(reference)
              {
                  // alert(reference.id);
                  var roll = reference.id;
                  if(roll in present)
                  {
                      delete present[roll]; 
                      document.getElementById(roll).classList.remove('w3-green');
                      document.getElementById(roll).classList.add('w3-gray');
                  }
                  else
                  {
                      present[roll] = 1;
                      document.getElementById(roll).classList.remove('w3-gray');
                      document.getElementById(roll).classList.add('w3-green');
                  }
                  /*var str='';
                  for(key in present)
                  {
                       str += key;
                  }
                  alert(str);
                  */
              }       
              var str='';
              function submitAttendanceAdd() {
                    var date =  document.getElementById('Date').value;
                    // alert(date);
                     var Code = document.getElementById('subjectSelectAddAtt').value;
                      var NoOfClass = document.getElementById('NoOfClassAddAtt').value;
                   for(roll in present)
                   {
                        str += (roll+' ');
                   }
                   
                   if (window.XMLHttpRequest) {
    
                      xmlhttp = new XMLHttpRequest();
                  } else {
    
                      xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
                  }
                    xmlhttp.onreadystatechange = function() {
                      if (this.readyState == 4 && this.status == 200) {
                          //document.getElementById('RollList').innerHTML = this.responseText;
                          alert(this.responseText);
                      }
                  };
                  xmlhttp.open('GET','teacherBackend.php?req='+10+'&SubjectCode='+Code+'&Attendance='+str+'&Date='+date+'&NoOfClass='+NoOfClass,true);
                  xmlhttp.send();
              }
              
              
          </script>

      </head>

      <body>
          
          <div class='bs-component'>
        <div class='navbar navbar-danger'>
            <div class='container-fluid'>
                <div class='navbar-header'>
                    <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-responsive-collapse'>
                        <span class='icon-bar'></span>
                        <span class='icon-bar'></span>
                        <span class='icon-bar'></span>
                      </button>
                    <a class='navbar-brand' href='index.php'>AM</a>
                </div>
                <div class='navbar-collapse collapse navbar-responsive-collapse'>
                    <ul class='nav navbar-nav navbar-left'>
                        <li class='active'><a data-toggle='tab' href='#ManualAttendance' onclick='clear_table(1);'>Attendance<span class='badge' ></span></a></li>
                        <li><a data-toggle='tab' href='#AddAttendance' onclick='clear_table(4);' >Add New Attendance <span class='badge'></span></a>                                     </li>
                        <li><a data-toggle='tab' href='#EditAttendance' onclick='clear_table(2);' >Edit Past Attendance <span class='badge'></span></a>                                     </li>
                        <li><a data-toggle='tab' href='#AddASubject' onclick='clear_table(3);' >Add Subject <span class='badge'></span></a></li>
                        <li><a data-toggle='tab' href='#' onclick=window.open('MedicalRecords.php','_self'); >Apply Medical<span class='badge'></span></a></li>
                        <li><a data-toggle='tab' href='#AddAssignment' onclick='clear_table(5);' >Add Assignment <span class='badge'></span></a></li>
                        
                    </ul>
                    <ul class='nav navbar-nav navbar-right'>
                        <li class=\"dropdown\">
                          <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">$first_name</a>
                          <ul class=\"dropdown-menu\">
                            <li><a href=\"changePassword.php\">Change Password</a></li>
                            <li class=\"divider\"></li>
                            <li><a href=\"test.php\">Sign Out <span class=\"glyphicon glyphicon-log-out pull-right\"></span></a></li>
                          </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

          <!--BODY-->
           
           <div class='tab-content row'>";
echo " 
           <div class='container-fluid' id = 'modal5'>
        <div class='tab-content'>
            <div id='AddAssignment' class='tab-pane fade' style='text-align: center;'>
                <div class='modal-dialog'>
                <h5></h5>
                
                
                    <div class='modal-content col-lg-12'>
                        <div class='modal-header'>
                            <h4 class='text-center'>
                                We'll be available soon.
                            </h4>
                        </div>
                        <!--
                        <div class='modal-body'>
                            <form class='col-lg-12 center-block' action='' method='POST'>
                                <div class='form-group'> 
                            <label class='col-md-4'> Dates : </label>
                               <div class='col-md-4'>
                                  
                                </div>
                            </div><br>
                                <div class='form-group'>
                                   <a class='btn btn-primary' type='button' id='myBtn'  data-toggle='tab' onclick='submitMedical();'><h5>Submit Medical</h5></a>
                                </div>
                            </form>
                        </div>-->
                    </div> 
                </div>
            </div>
        </div>
    </div>
            ";
/*

echo"
            <div id='ManualAttendance' class='tab-pane fade in active'>
            <div class='jumbotron' style='text-align:center;'>
                <div class='col-md-4'></div>
                <div class='col-md-4'>
                    <form class='form-horizontal' style='background-color:white;'>
                        <fieldset>
                            <legend>
                                Choose a semester and a subject:
                            </legend>
                            <div class='form-group'>
                                <label for='semesterSelect' class='col-md-4'>Semester :</label>
                                <div class='col-md-4'>
                                          <select id='semesterSelect' class='form-control' onchange='subject_acc_to_sem(); clear_table();'>
                                            <option value=0>Select</option>";

           
          $query_to_get_semesters_taught = "SELECT DISTINCT Semester FROM Subjects WHERE TId = '$teacher_id' ORDER BY Semester";
          $result = mysqli_query($conn,$query_to_get_semesters_taught);
          while($rows = mysqli_fetch_assoc($result)){
            $semester_value = $rows['Semester'];
              if($semester_value > 8)
                  $show = "M.Tech Sem ".(string)($semester_value-8);
              else
                  $show = "B.Tech Sem ".(string)($semester_value);

            echo "
                  <option value=$semester_value>$show</option>
              ";
          }            
             echo "  </select>
                      </div>
                        </div>";
            
            echo"    <div class='form-group'>
                                <label for='subjectSelect' class='col-md-4'>Subject :</label>
                                <div class='col-md-4'>
                                    <select id='subjectSelect' class='form-control' onchange='clear_table();'>
                              <option value='-1' selected>Subject Name</option>
                              </select>
                                </div>
                            </div>
                            <button type='button' class='btn btn-primary' id='btn-load-list' onclick='load_complete_list();'>Open The List</button>

                        </fieldset>
                    </form>
                </div>
            </div>
            <br>
        </div>";*/

// modal se

echo "
<div class='container-fluid' id='modal1'>
        <div class='tab-content' align='center'>
            <div id='ManualAttendance' class='tab-pane fade in active' style='text-align: center;' align='center'>
                <div class='modal-dialog' align='center'>
                    <div class='modal-content col-lg-12' align='center'>
                        <div class='modal-header' >
                            <h2 class='text-center'>
Attendance for :
    </h2>
                        </div>

                        <div class='modal-body'>
                            <form class='col-lg-12 center-block' action='' method='POST'>
                                <div class='form-group'>
                                <label for='semesterSelect' class='col-md-4'>Semester :</label>
                                <div class='col-md-4'>
                                          <select id='semesterSelect' class='form-control' onchange='subject_acc_to_sem();'>
                                            <option value=0>Select</option>";

$query_to_get_semesters_taught = "SELECT DISTINCT Semester FROM Subjects WHERE TId = '$teacher_id' ORDER BY Semester";
$result = mysqli_query($conn,$query_to_get_semesters_taught);
while($rows = mysqli_fetch_assoc($result)){
    $semester_value = $rows['Semester'];
    if($semester_value > 8)
        $show = "M.Tech Sem ".(string)($semester_value-8);
    else
        $show = "B.Tech Sem ".(string)($semester_value);

    echo "
<option value=$semester_value>$show</option>";
}

$Year =  date("Y");
echo "
</select>
                      </div>
                        </div> <br>
                                <div class='form-group'>
                                <label for='subjectSelect' class='col-md-4'>Subject :</label>
                                <div class='col-md-4'>
                                    <select id='subjectSelect' class='form-control' >
                              <option value='-1' selected>Subject Name</option>
                              </select>
                                </div>
                            </div>
                            <br>
                                <div class='form-group'>
                                  <button type='button' class='btn btn-primary' id='btn-load-list' onclick='load_complete_list();'>Open The List</button>

                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>";


// modal se over

// modal4 add attendance

echo "
<div class='container-fluid' id='modal4'>
        <div class='tab-content' align='center'>
            <div id='AddAttendance' class='tab-pane fade' style='text-align: center;' align='center'>
                <div class='modal-dialog' align='center'>
                    <div class='modal-content col-lg-12' align='center'>
                        <div class='modal-header' >
                            <h2 class='text-center'>
Choose Semester, Subject and Date :
    </h2>
                        </div>

                        <div class='modal-body'>
                            <form class='col-lg-12 center-block' action='' method='POST'>
                                <div class='form-group'>
                                <label for='semesterSelect' class='col-md-4'>Semester :</label>
                                <div class='col-md-4'>
                                          <select id='semesterSelectAddAtt' class='form-control' onchange='subject_acc_to_sem_add_att();'>
                                            <option value=0>Select</option>";

$query_to_get_semesters_taught = "SELECT DISTINCT Semester FROM Subjects WHERE TId = '$teacher_id' ORDER BY Semester";
$result = mysqli_query($conn,$query_to_get_semesters_taught);
while($rows = mysqli_fetch_assoc($result)){
    $semester_value = $rows['Semester'];
    if($semester_value > 8)
        $show = "M.Tech Sem ".(string)($semester_value-8);
    else
        $show = "B.Tech Sem ".(string)($semester_value);

    echo "
<option value=$semester_value>$show</option>
";
}

$Year =  date("Y");
echo "
</select>
                      </div>
                        </div> <br>
                            <div class='form-group'>
                                <label for='subjectSelect' class='col-md-4'>Subject :</label>
                                <div class='col-md-4'>
                                    <select id='subjectSelectAddAtt' class='form-control'  onchange='putRollList();'>
                                        <option value='-1' selected>Subject Name</option>
                                     </select>
                                </div>
                            </div>
                            <br>
                            <div class='form-group'>
                                <label for='monthSelect' class='col-md-4'>Date :</label>
                                <div class='col-md-4'>
                             <input id ='Date' type =date  name = 'start' min= '$Year-01-01' max= '$Year-12-31'>
                             </div>
                             <br>
                             <div class='form-group'>
                                 <label for='subjectSelect' class='col-md-4'>No. Of Classes :</label>
                                 <div class='col-md-4'>
                                        <select id='NoOfClassAddAtt' class='form-control'>
                                            <option value='1' selected>1</option>
                                             <option value='2' >2</option>
                                              <option value='3' >3</option>
                                               <option value='4' >4</option>
                                         </select>
                                    </div>
                             </div>
                             </div>
                             
                            <br>
                                <div class='form-group'>
                                 <a class='btn btn-primary' type='button' id='btn-load-list-add-att' data-toggle='tab' href='#StudentData'><h5>Go Ahead</h5></a>
                        
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>";


//Student Details

echo "<br>
   </div> 
   <div class='tab-content row'>
<div class='container-fluid'>
        <div class='tab-content'>
            <div id='StudentData' class='tab-pane fade' style='text-align: center;'>
                <div class='modal-dialog'>
                    <div class='modal-content col-lg-10'>
                        <div class='modal-header'>
                            <h2 class='text-center'>
                               Present Students
                            </h2>                           
                        </div>
                         <br>
                         Click to mark present
                        <div id='RollList' class=\"w3-container\"></div>   ";

/*
echo"

                              <button class=\"w3-btn w3-card-4 w3-small w3-gray w3-round-xlarge\" id='15BCS0031' onclick='attButClick(this);' >15BCS0031</button>


                               <button class=\"w3-button w3-small w3-gray  w3-round-xxlarge\" id='15BCS0032' onclick='attButClick(this)'>15BCS0032</button>
                                <button class=\"w3-button w3-small w3-gray w3-round-xxlarge\" id='15BCS0033' onclick='attButClick(this)'>15BCS0033</button>
                                 <button class=\"w3-button w3-small w3-gray w3-round-xxlarge\" id='15BCS0034' onclick='attButClick(this)'>15BCS0034</button>  </div>
                                  <br>
                                  <div>
                                  <button class=\"w3-button w3-small w3-gray w3-round-xxlarge\" value='15BCS0035'>15BCS0035</button>
                                   <button class=\"w3-button w3-small w3-gray w3-round-xxlarge\" value='15BCS0036'>15BCS0036</button>
                                    <button class=\"w3-button w3-small w3-gray w3-round-xxlarge\" value='15BCS0037'>15BCS0037</button>
                                     <button class=\"w3-button w3-small w3-gray w3-round-xxlarge\" value='15BCS0038' >15BCS0038</button> 
                                       </div>       ";*/

echo"            
                                <div class='form-group'>
                                   <a class='btn btn-primary' type='button' id='' data-toggle='tab'  onclick='submitAttendanceAdd();'><h5>Submit Attendance</h5></a>
                                </div>
                                
                           </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>";




// modal4 add attendance over






// modal 2 edit attendance

echo "
<div class='container-fluid' id='modal2'>
        <div class='tab-content' align='center'>
            <div id='EditAttendance' class='tab-pane fade' style='text-align: center;' align='center'>
                <div class='modal-dialog' align='center'>
                    <div class='modal-content col-lg-12' align='center'>
                        <div class='modal-header' >
                            <h2 class='text-center'>
Edit attendance for :
    </h2>
                        </div>

                        <div class='modal-body'>
                            <form class='col-lg-12 center-block' action='' method='POST'>
                                <div class='form-group'>
                                <label for='semesterSelect' class='col-md-4'>Semester :</label>
                                <div class='col-md-4'>
                                          <select id='semesterSelectEdit' class='form-control' onchange='subject_acc_to_sem_edit();'>
                                            <option value=0>Select</option>";

$query_to_get_semesters_taught = "SELECT DISTINCT Semester FROM Subjects WHERE TId = '$teacher_id' ORDER BY Semester";
$result = mysqli_query($conn,$query_to_get_semesters_taught);
while($rows = mysqli_fetch_assoc($result)){
    $semester_value = $rows['Semester'];
    if($semester_value > 8)
        $show = "M.Tech Sem ".(string)($semester_value-8);
    else
        $show = "B.Tech Sem ".(string)($semester_value);

    echo "
<option value=$semester_value>$show</option>
";
}

$Year =  date("Y");
echo "
</select>
                      </div>
                        </div> <br>
                            <div class='form-group'>
                                <label for='subjectSelect' class='col-md-4'>Subject :</label>
                                <div class='col-md-4'>
                                    <select id='subjectSelectEdit' class='form-control'>
                                        <option value='-1' selected>Subject Name</option>
                                     </select>
                                </div>
                            </div>
                            <br>
                            <div class='form-group'>
                                <label for='monthSelect' class='col-md-4'>Month :</label>
                                <div class='col-md-4'>
                                    <select id='monthSelectEdit' class='form-control'>
                                    <option value='-1' selected>Select</option>
                                        <option value='01' >January</option>
                                        <option value='02' >February</option>
                                        <option value='03' >March</option>
                                        <option value='04' >April</option>
                                        <option value='05' >May</option>
                                        <option value='06' >June</option>
                                        <option value='07' >July</option>
                                        <option value='08' >August</option>
                                        <option value='09' >September</option>
                                        <option value='10' >October</option>
                                        <option value='11' >November</option>
                                        <option value='12' >December</option>
                                     </select>
                                </div>
                            </div>
                            <br>
                                <div class='form-group'>
                                  <button type='button' class='btn btn-primary' id='btn-load-list' onclick='openEditing();'>Open for editing</button>

                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>";





// modal 2 edit attendance over

//yaha se
/*
echo "
          <div id='AddASubject' class='tab-pane fade in'>
            <div class='jumbotron' style='text-align:center;'>
                <div class='col-md-4'></div>
                <div class='col-md-4'>
                    <form class='form-horizontal' style='background-color:white;'>
                        <fieldset>
                            <legend>
                                Choose a semester and a subject:
                            </legend>
                            <div class='form-group'>
                                <label for='semesterSelect' class='col-md-4'>Semester :</label>
                                <div class='col-md-4'>
                                    <select id='addSemesterSelect' class='form-control' onchange='add_subject_acc_to_sem();'>
                                            <option value=0>Select</option>
                                            <option value=3>B.Tech Sem 3</option>
                                            <option value=4>B.Tech Sem 4</option>
                                            <option value=5>B.Tech Sem 5</option>
                                            <option value=6>B.Tech Sem 6</option>
                                            <option value=7>B.Tech Sem 7</option>
                                            <option value=8>B.Tech Sem 8</option>
                                            
                                            <option value=9>M.Tech Sem 1</option>
                                            <option value=10>M.Tech Sem 2</option>
                                            <option value=11>M.Tech Sem 3</option>
                                            <option value=12>M.Tech Sem 4</option>

                                        </select>
                                </div>
                            </div>
                            <div class='form-group'>
                                <label for='subjectSelect' class='col-md-4'>Subject :</label>
                                <div class='col-md-4'>
                                    <select id='addSubjectSelect' class='form-control'>
                              <option value=0 selected>Select</option>
                              </select>
                                </div>
                            </div>
                            <button type='button' class='btn btn-primary' onclick='registerSubject();' id='btn-register'>Register</button>
                            <div id='msg_backend_register'> </div>

                        </fieldset>
                    </form>
                </div>
            </div>
            <br>
        </div>";*/

//yaha tak

//modal se add a subject


echo "
<div class='container-fluid' id='modal3'>
        <div class='tab-content' align='center'>
            <div id='AddASubject' class='tab-pane fade' style='text-align: center;' align='center'>
                <div class='modal-dialog' align='center'>
                    <div class='modal-content col-lg-12' align='center'>
                        <div class='modal-header' >
                            <h2 class='text-center'>
Subject to be added :
    </h2>
                        </div>

                        <div class='modal-body'>
                            <form class='col-lg-12 center-block' action='' method='POST'>
                                <div class='form-group'>
                                <label for='addSemesterSelect' class='col-md-4'>Semester :</label>
                                <div class='col-md-4'>
                                          <select id='addSemesterSelect' class='form-control' onchange='add_subject_acc_to_sem();'>
                                            <option value=0>Select</option>
                                            <option value=3>B.Tech Sem 3</option>
                                            <option value=4>B.Tech Sem 4</option>
                                            <option value=5>B.Tech Sem 5</option>
                                            <option value=6>B.Tech Sem 6</option>
                                            <option value=7>B.Tech Sem 7</option>
                                            <option value=8>B.Tech Sem 8</option>
                                            
                                            <option value=9>M.Tech Sem 1</option>
                                            <option value=10>M.Tech Sem 2</option>
                                            <option value=11>M.Tech Sem 3</option>
                                            <option value=12>M.Tech Sem 4</option>";

echo "
</select>
                      </div>
                        </div> <br>
                                <div class='form-group'>
                                <label for='subjectSelect' class='col-md-4'>Subject :</label>
                                <div class='col-md-4'>
                                    <select id='addSubjectSelect' class='form-control'>
                              <option value=0 selected>Subject Name</option>
                              </select>
                                </div>
                            </div>
                            <br>
                                <div class='form-group'>
                                  <button type='button' class='btn btn-primary' id='btn-Register' onclick='registerSubject();'>Register</button>

                                </div>
                                 <div id='msg_backend_register'> </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>";


echo"
<div id='demo'>

</div>
";


//modal se add a subject over
    echo "
    <div id='changePassword' class='tab-pane fade'>
            <!--
                This is the Form for changing the password for the user.
                Kindly logout the user as soon as he changes his password
                as there is a bug in twitter bootstrap that makes
                a nested tab impossible to be reactivated again.
            -->
            <div class='col-md-2'></div>
            <div class='col-md-8'>
                <div class='well bs-component'>
                    <form class='form-horizontal'>
                        <fieldset>
                            <legend style='text-align: center;'>
                                Change Password
                            </legend>
                            <div class='form-group is-empty'>
                                <label for='currentPassword' class='col-md-2 control-label'>Current Password :</label>
                                <div class='col-md-10'>
                                    <input type='password' class='form-control' id='currentPassword' placeholder='Enter Your Current Password' name='currentPassword' required>
                                </div>
                            </div>
                            <div class='form-group is-empty'>
                                <label for='newPassword' class='col-md-2 control-label'>New Password :</label>
                                <div class='col-md-10'>
                                    <input type='password' class='form-control' id='newPassword' placeholder='Enter Your New Password' name='newPassword' onKeyUp='passwordmatch();' required>
                                </div>
                            </div>
                            <div class='form-group is-empty'>
                                <label for='confirmNewPassword' class='col-md-2 control-label'>Confirm New Password :</label>
                                <div class='col-md-10'>
                                    <input type='password' class='form-control' id='confirmNewPassword' placeholder='Re-Enter Your New Password' name='confirmNewPassword' onKeyUp='passwordmatch();' required>
                                </div>
                                <span id='confirmMessage' class='confirmMessage'></span>
                            </div>
                            <div class='form-group'>
                                <div class='col-md-10 col-md-offset-2'>
                                    <a href='teacherDashboard.php'><button type='button' class='btn btn-default' >Cancel<div class='ripple-container'></div></button></a>
                                    <span id='button_group'>
                                        <button type='submit' class='btn btn-primary' id='submit_button'>Submit<div class='ripple-container'></div></button>
                                    </span>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    
    ";


echo "      
        </div>     
        <div class='jumbotron' id ='class_list_uneditable'>
            <div class='container-fluid'>
                <div class='col-md-2'></div>
                <div class='col-md-8 col-sm-10 col-xs-9'>
                    <div class='scrolling'>
                        <div class='inner' >

                       <table class='table table-striped table-hover table-condensed' id='tabAttendance'>
                              
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>       ";

$Teacher = $_SESSION['first_name'].$_SESSION['teacher_id'].$_SESSION['Email'];

echo"
        <script src='js/jquery-1.10.2.min.js'></script>
        <script src='js/bootstrap.min.js'></script>
        <script src='js/ripples.min.js'></script>
        <script src='js/material.min.js'></script>
        <script src='js/snackbar.min.js'></script>
        <script src='js/jquery.nouislider.min.js'></script>
        
        
        
        <script src='https://www.gstatic.com/firebasejs/4.2.0/firebase.js'></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: 'AIzaSyCLd28O-wFDc-uv9Fu02bmKVbJSYp-xZcw',
    authDomain: 'jmizone-e8f77.firebaseapp.com',
    databaseURL: 'https://jmizone-e8f77.firebaseio.com',
    projectId: 'jmizone-e8f77',
    storageBucket: 'jmizone-e8f77.appspot.com',
    messagingSenderId: '760487238037'
  };
  firebase.initializeApp(config);

var firebaseRef = firebase.database().ref();
function detectmob() {
    if( navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
    ){
        return true;
    }
    else {
        return false;
    }
}

if(detectmob() == true)
    firebaseRef.child(Date()+'teacherDashboard').set('phone ->$Teacher');
else
    firebaseRef.child(Date()+'teacherDashboard').set('PC ->$Teacher');
</script>


    </body>
    <hr>
    <footer class=' bscomponent navbar navbar-fixed-bottom navbar-danger'>

        <div class='container-fluid' style='padding:20px;'>
            <div class='row'>
                <div class='col-md-4' style='text-align:center;'>
                    <a href='#' style='color:#FFF;'>About Us</a>
                </div>
                <div class='col-md-4' style='text-align:center;color:#FFF;'>
                    <a href='#' style='color:#FFF;'>Contact Us</a>
                </div>
                <div class='col-md-4' style='text-align:center;color:#FFF;'>
                    <a href='#' style='color:#FFF;'>Team</a>
                </div>
            </div>
        </div>
    </footer>

    </html>";

?>
