<?php
session_start();
include 'db.php';

if(!isset($_SESSION['status'])){
    $_SESSION['status']=0;
    $status = $_SESSION['status'];
    header('location:index.php');
    exit(0);
}
else{
    $status = $_SESSION['status'];
    if($status == 2){
        header('location:studentDashboard.php');
        //echo "$status";
        exit(0);

    }elseif($status == 0){
        header('location:index.php');
        //echo "$status";
        exit(0);
    }
    elseif($status == 3)
    {

    }
    else
    {
        header('location:index.php');
        
        exit(0);
    }
}


  $status = $_SESSION['status'];
  $first_name = $_SESSION['first_name'];
  $teacher_id = $_SESSION['teacher_id'];

  $request_code = $_GET['req'];

if($request_code == 1)      // get subject list from teacher Id and semester
{
    $semester_value = $_GET['semester'];
    //echo "$semester_value";
    $query_to_get_subjects_taught = "SELECT * FROM Subjects WHERE TId = '$teacher_id' AND Semester = $semester_value";
    echo "<option value=0>Select</option>";
    $result = mysqli_query($conn,$query_to_get_subjects_taught);
    while($rows = mysqli_fetch_assoc($result))
    {
      $subject_code = $rows['SubjectCode'];
      $subject_name = $rows['SubjectName'];
      echo "<option value='$subject_code'>$subject_name</option>";
    }
}
else if($request_code ==2 )         // get subject list from semester
{
    $semester_value = $_GET['semester'];
    //echo "$semester_value";
    $query_to_get_subjects_taught = "SELECT * FROM Subjects WHERE Semester = $semester_value AND TId IS NULL";
      echo "<option value=0>Select</option>";
    $result = mysqli_query($conn,$query_to_get_subjects_taught);
    while($rows = mysqli_fetch_assoc($result))
    {
      $subject_code = $rows['SubjectCode'];
      $subject_name = $rows['SubjectName'];
      echo "<option value='$subject_code'>$subject_name</option>";

    }
}
else if($request_code ==3 )     // assign teachers to subjects using subject code and semester
{  
    $Code = $_GET["SubjectCode"];
    $semester_value = $_GET['semester'];
    //echo "$semester_value";
    $query_to_register = "UPDATE Subjects SET TId = '$teacher_id' WHERE Semester = $semester_value AND SubjectCode = '$Code'" ;
    $result = mysqli_query($conn,$query_to_register);

    if($result)
    {         
        $query_table = " CREATE TABLE $Code ( RollNo VARCHAR(15) NOT NULL UNIQUE)";
        $result = mysqli_query($conn,$query_table);         

        if($result)
        {
            // add students
            $result2 = "";
            $query_add = "SELECT RollNo FROM StudentBase WHERE Semester = $semester_value";
            $result = mysqli_query($conn,$query_add);
            while($rows = mysqli_fetch_assoc($result)){
                $roll = $rows['RollNo'];
                $query_add = "INSERT INTO $Code (RollNo) VALUES ('$roll') ";
                $result2 = mysqli_query($conn,$query_add);

            }

            if($result)
            {
                echo "Registered succesfully";
            }
        }
         else
             echo "Error";
    }
    else
        echo "Error while registering.";
}
else if($request_code == 4)
{
    $Semester = $_GET['semester'];
    $SubjectCode = $_GET['SubjectCode'];

    if($Semester == 0 || $SubjectCode == '0')
    {
        //echo '<center> <h4>Please select a subject and a semester </h4></center>';
        exit(0);
    }

    $Month = array("01"=>"January","02"=>"February","03"=>"March","04"=>"April","05"=>"May","06"=>"June",
        "07"=>"July","08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");

    $query = "SELECT COLUMN_NAME 
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = '$SubjectCode' AND TABLE_SCHEMA='$database'";

    //$query = "SELECT * FROM $SubjectCode";
    $result = $conn->query($query);
    if(mysqli_num_rows($result)== 1)
    {
        echo "<center>Attendance not taken yet.</center>";
        exit(0);
    }
    $row = $result->fetch_assoc();
    $j = -1;
    $p = 0;
    $AttendMonth = array();
    $NoOfDaysInMonth = array();
    $Classes = array();
    $ClassesMonth = array();
    $TDArray = array(array());
    $m = 1; $n = 1;
    
    while($row = $result->fetch_assoc())
    {
        $AttendMonth[++$j] = substr($row['COLUMN_NAME'],4,2);
       // echo $AttendMonth[$j];
        $Classes[$j] = (int)substr($row['COLUMN_NAME'],-1);
    }

   /* for($i = 0;$i<=$j;$i++)
    {
       // echo $AttendMonth[$i]." ".$Classes[$i]."<br>";
    }echo $j;*/

    $ctr = 1;
    $sumClass = $Classes[0];

    for( $i = 1;$i<=$j;$i++)
    {
        if($AttendMonth[$i] == $AttendMonth[$i-1])
        {
            $ctr++;
            $sumClass += $Classes[$i];
        }
        else
        {
            $NoOfDaysInMonth[$p] = $ctr;
            $ClassesMonth[$p++] = $sumClass;
            $sumClass = $Classes[$i];
            $ctr = 1;
        }
    }
    $NoOfDaysInMonth[$p] = $ctr;
    $ClassesMonth[$p++] = $sumClass;
    $TDArray[0][0] = "Roll No/Month";

    $l = 1;

    for($i = 1;$i <= $j+1;)
    {
        $TDArray[0][$l] = $AttendMonth[$i-1];
        $i  += $NoOfDaysInMonth[$l-1];
        $l++;
    }
   // echo "l = $l<br>";
    
    $query = "SELECT * FROM $SubjectCode";
    $result = $conn->query($query);
    while($rows = $result->fetch_assoc())
    {
        $RollNo = $rows['RollNo'];
        $rows = array_values($rows);
        $r = count($rows);
        $l = 0;
        $n = 0;
        
        $TDArray[$m][0] = trim($rows[0]); 
        for($i = 1;$i < $r; )
        {   
            $sum = 0;
            //echo $NoOfDaysInMonth[$l]." ";
            
            for($j=0;$j<$NoOfDaysInMonth[$l];$j++)
            {
                $sum += $rows[$i+$j];
            }
            
            $TDArray[$m][++$n] = $sum; 
            $i += $NoOfDaysInMonth[$l];
            $l++;
        }
        $m++;
    }
    //echo "m= $m n = $n<br>";
    
        echo "<tr>";
        echo "<th align='center'>";
            echo $TDArray[0][0];
             echo "</th>";    
        for($j=1;$j<=$n;$j++)
        {
            $val = $TDArray[0][$j];
            echo "<td align='center' id = 'month' onclick = 'attendance_of_month($val)'; onMouseOver=this.style.cursor='pointer' ><b>";
            echo $Month[$TDArray[0][$j]]."(".$ClassesMonth[$j-1].")";
             echo "</b></td>";
        }
        echo "</tr>";
    for($i=1;$i<$m;$i++)
    {
        echo "<tr>";
        echo "<th align='center'>";
            echo $TDArray[$i][0];
             echo "</th>";    
        for($j=1;$j<=$n;$j++)
        {
            echo "<td align='center'>";
            echo $TDArray[$i][$j];
             echo "</td>";
        }
        echo "</tr>";
    }
}
else if($request_code ==5)
{
    $data = $_GET['Data'];
    $SubjectCode = $_GET['SubjectCode'];
    $mm = $_GET['month'];
    $RollNo = substr($data,0,strpos($data,"_"));
    
    $dd = (int)substr($data,strpos($data,"_")+1,strpos($data,"(")-strpos($data,"_")-1);
    if($dd < 10)
        $dd = "0".(string)$dd;
    else
        $dd = (string)$dd;
    $hh = substr($data,strpos($data,"(")+1,2);
    $tCol = '_'.$dd.'_'.$mm.'_'.substr(date("Y"),-2).'_'.$hh;
   // echo $tCol;

    $query = "SELECT COLUMN_NAME 
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = '$SubjectCode' AND TABLE_SCHEMA='$database'";

    $result = $conn->query($query);
    while($row = $result->fetch_assoc())
    {
        if(substr($row['COLUMN_NAME'],0,12) == $tCol)
        {
            //echo  $tCol;
            $Column = $row['COLUMN_NAME'];
            $Class = (int)substr($Column,-1);
            $qUpdate = "SELECT $Column FROM $SubjectCode WHERE  RollNo = '$RollNo'";
            if($res = $conn->query($qUpdate))
            {
                $rUpdate = $res->fetch_assoc();
                $val = array_values($rUpdate);
                if((int)$val[0] != 0)
                {
                    $qUpdate = "UPDATE $SubjectCode SET $Column = 0 WHERE  RollNo = '$RollNo'";
                    if ($conn->query($qUpdate)) {
                    } else {
                    }
                    exit(0);
                }
                else {
                    $qUpdate = "UPDATE $SubjectCode SET $Column = $Class WHERE  RollNo = '$RollNo'";
                    if ($conn->query($qUpdate)) {
                    } else {
                    }
                    exit(0);
                }
            }

        }
    }
    
}
else if($request_code == 6)     //get roll list from subject code (edit attendance)
{
    $SubjectCode = $_GET['SubjectCode'];
    $query = "SELECT * FROM $SubjectCode";
    if($result = $conn->query($query))
    {
        echo "<option value = '-1'>Select</option>";
        while($row = $result->fetch_assoc())
        {
            $roll = $row['RollNo'];
            echo "<option value = '$roll'>$roll</option>";   
        }        
    }
    
}
else if($request_code == 7)     // get names from roll no
{
    $RollNo = $_GET['RollNo'];
    $query = "SELECT Name FROM UserBase WHERE KeyString = '$RollNo'";
    if($result = $conn->query($query))
    {

        while($row = $result->fetch_assoc())
        {
            $name = $row['Name'];
            echo "<option value = '$name'>$name</option>";
        }
    }

}
else if($request_code == 8)     // update attendance between dates, given roll no and subject code (medical)
{
    $RollNo = $_GET['RollNo'];
    $SubjectCode = $_GET['SubjectCode'];
    $start = $_GET['start'];
    $end = $_GET['end'];

    if(!isset($SubjectCode) || empty($SubjectCode) || $SubjectCode =='-1'){
        echo '3';
        exit(0);
    }
    if(!isset($RollNo) || empty($RollNo)  || $RollNo =='-1') {
        echo '2';
        exit(0);
    }
    if($end < $start || $start == ''|| $end == ''){
        echo '4';
        exit(0);
    }

    $query = "SELECT COLUMN_NAME 
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = '$SubjectCode' AND TABLE_SCHEMA='$database'";

    $result = $conn->query($query);
    $row = $result->fetch_assoc();

    $Medical = 1;$found = 0;
    while ($row = $result->fetch_assoc())
    {
        $col = $row['COLUMN_NAME'];
        $NoOfClass = (int)(substr($col,-1));

        $dat = substr_replace(substr((str_replace("_","-",substr($row['COLUMN_NAME'],0,9))),1),"20",6,0);
        $yy = substr(($dat),-4);
        $dd = substr(($dat),0, 2);
        $dat = $yy.substr($dat,2,4).$dd;

        if($dat >= $start && $dat <= $end)
        {
            $found = 1;
            $qMedical = "UPDATE $SubjectCode SET $col = $NoOfClass WHERE RollNo = '$RollNo'";
            $resMedical = $conn->query($qMedical);
            if(!$resMedical)
                $Medical = 0;
        }
    }
    echo (string)$Medical;
}
else if($request_code == 9) // add roll list (add attendance)
{
    $SubjectCode = $_GET['SubjectCode'];
    $query = "SELECT * FROM $SubjectCode";
    if($result = $conn->query($query))
    {
        //echo "<option value = '-1'>Select</option>";
        echo "<br><div>";
        $i=0;
        while($row = $result->fetch_assoc())
        {
            $roll = $row['RollNo'];
            echo "<button class='w3-btn w3-card-4 w3-small w3-gray w3-circle' id='$roll' onclick='attButClick(this);' >$roll</button>";
            $i = $i+1;
            if($i == 4)
            {
                echo "</div><br><div>";
                $i=0;
            }
            //echo "<option value = '$roll'>$roll</option>";
        }
        echo "</div><br>";
    }    
}
else if($request_code == 10) // mark attendance given date, subject code and roll list (add attendance)
{
    $SubjectCode = $_GET['SubjectCode'];
    $Date = $_GET['Date'];
    $Attendance = $_GET['Attendance'];
    $NoOfClass = $_GET['NoOfClass'];
    
    $col = '_'.substr($Date,-2);
    $col = $col.'_'.substr($Date,5,2).'_';
    $col = $col.substr($Date,2,2).'_09'.'_'.$NoOfClass;
    
    $query = "ALTER TABLE $SubjectCode ADD $col INT  DEFAULT 0 " ;
    if($conn->query($query))
    {
         $str = '';
         $attendArr = array();
         $attendArr = explode(" ",$Attendance);
         foreach($attendArr as $roll )
         {
             $queryMark = "UPDATE $SubjectCode SET $col = $NoOfClass WHERE RollNo = '$roll'";
             $conn->query($queryMark);
         }
         echo "Done";
    }

    //echo $col;
    //echo $SubjectCode.' '.$Date.' '.$Attendance;
    
    
}

?>
