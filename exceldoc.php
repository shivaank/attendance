<?php

//header( "Content-Type: application/vnd.ms-excel" );
//header( "Content-disposition: attachment; filename=spreadsheet.xls" );

session_start();
if(!isset($_SESSION['status'])){
	$_SESSION['status']=0;
	$status = $_SESSION['status'];
	header('location:index.php');
	exit(0);
}
else{
	$status = $_SESSION['status'];
	if($status == 2){
		header('location:studentDashboard.php');
		echo "$status";
		exit(0);

	}elseif($status == 0){
		header('location:index.php');
		echo "$status";
		exit(0);
	}
	elseif($status == 3){}
	else
	{
		header('location:index.php');
		echo "$status";
		exit(0);
	}
}

include 'db.php';
$Month = array("01"=>"January","02"=>"February","03"=>"March","04"=>"April","05"=>"May","06"=>"June","07"=>"July","08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");

$SubjectCode = $_GET['subject'];
$mon = $_GET['month'];
$month = $Month[$_GET['month']];

$query = "SELECT COLUMN_NAME 
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = '$SubjectCode' AND TABLE_SCHEMA='$database'";
$result = $conn->query($query);
$row = $result->fetch_assoc();
$j = 0;
while($row = $result->fetch_assoc())
{
	++$j;
	$rowMon = substr($row['COLUMN_NAME'],4,2);
	if($rowMon == $mon)
	{
		break;
	}
}
$Dat = array();$rowAttend = array(); $TotCl = array();
$TDArray = array(array());
$LenArr = array();
$la = 0;
$d = 0;

$Year = (int)substr($row['COLUMN_NAME'],7,2);

$n = (int)substr($row['COLUMN_NAME'],1,2);

$Dat[$d++] = $n;

$TotCl[0] = (int)substr($row['COLUMN_NAME'],-1);
$LenArr[0] = 1;

while($row = $result->fetch_assoc())
{
	$rowMon = substr($row['COLUMN_NAME'],4,2);
	if($rowMon == $mon)
	{
		$n = (int)substr($row['COLUMN_NAME'],1,2);
		if($Dat[$d-1] == $n)
		{
			$LenArr[$la]++;
			$TotCl[$d-1] += (int)substr($row['COLUMN_NAME'], -1);
		}
		else
		{
			$la++;
			$LenArr[$la] = 1;
			$Dat[$d++] = $n;
			$TotCl[$d - 1] = (int)substr($row['COLUMN_NAME'], -1);
		}
	}
	else
	{
		break;
	}
}



$m = -1;
$ClassPresent = array();
$query = "SELECT * FROM $SubjectCode";
$result = $conn->query($query);
while($row = $result->fetch_assoc())
{
	$n = 0;

	$rowAttend = array_values($row);
	$TDArray[++$m][$n++] = $rowAttend[0];
	$ClassPresent[$m] = 0;

	$z = -1;
	$f = $j;
	for($i = 0;$i<$d;$i++)
	{
		$str="";
		$limit = $LenArr[$i];
		$g = 0;
		$num = 0;
		for(;$g<$limit;$f++,$g++) {
			$num += $rowAttend[$f];
			$ClassPresent[$m] += $rowAttend[$f];

		}
		// $j += $limit;

		for($a = 0;$a<$num;$a++)
			$str.="P";

		$tot = $TotCl[$i]-$num;

		for($a = 0;$a<$tot;$a++)
			$str.="A";

		$TDArray[$m][$n++] = $str;//$TotCl[$i]."_".$rowAttend[$f];
	}
	//echo "<br>";
}
$m++;
$sum = array_sum($TotCl);



$NameStd = array();
for($i = 0;$i<$m;$i++)
{
	//echo $TDArray[$i][0]." ";
	$roll = $TDArray[$i][0];
	$query = "SELECT FirstName, MiddleName, LastName FROM StudentBase WHERE RollNo = '$roll'";
	//$query = "SELECT FirstName, MiddleName, LastName FROM StudentBase WHERE RollNo = '$TDArray[$i][0]'";
	$result = $conn->query($query);
	$rowStd = $result->fetch_assoc();

	if($rowStd['MiddleName'] == null)
		$NameStd[$i] = $rowStd['FirstName'].' '.$rowStd['LastName'];
	else
		$NameStd[$i] = $rowStd['FirstName'].' '.$rowStd['MiddleName'].' '.$rowStd['LastName'];
	//echo $NameStd[$i]."<br>";

}
$query = "SELECT SubjectName, Semester FROM Subjects WHERE SubjectCode = '$SubjectCode'";
$result = $conn->query($query);
$rowSub = $result->fetch_assoc();
$Sub = $rowSub['SubjectName'];
$Semester = $rowSub['Semester'];

include_once('./phpexcel/Classes/PHPExcel/IOFactory.php');

//set the desired name of the excel file
$fileName = "$month$Year-$SubjectCode";

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Attendance Manager")->setLastModifiedBy("Attendance Manager")->setTitle("Computer Engineering Department")->setSubject("$Sub")->setDescription("Excel Sheet")->setKeywords("$month-$Year-Attendance")->setCategory("");

// Set active sheet index to the
// first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// fonts
$objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->applyFromArray(array('font' => array('size' => 10,'bold' => true,'color' => array('rgb' => '000000')),'alignment' => array(
	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
)));
 $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $objPHPExcel->getDefaultStyle()->applyFromArray($style);

$n = count($TDArray[0])+4;
for($i=0;$i<$n;$i++)
	$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);

// Add column headers
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Roll No')->setCellValue('B1', 'Name');
$d = count($Dat);

for($i = 0;$i<$d;$i++)
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($i+2,1,(string)$Dat[$i]);

$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($i+2,1,'Total');
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($i+3,1,'Percentage');

//Put each record in a new cell
$m = count($TDArray);
$n = count($TDArray[0]);

for($i=0; $i<$m; $i++){
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i+2, $TDArray[$i][0]);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i+2, $NameStd[$i]);

	for($j = 1;$j<$n;$j++) {
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j+1, $i+2, $TDArray[$i][$j]);
	}
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j+1, $i+2, $ClassPresent[$i].'/'.$sum);
	$perc = (string)((((int)$ClassPresent[$i])/((int)$sum))*100);
	$perc  = number_format((float) $perc, 1, '.', '');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j+2, $i+2, $perc."%");

}

// Set worksheet title
$objPHPExcel->getActiveSheet()->setTitle($fileName);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $fileName . '.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

?>