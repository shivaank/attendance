<?php
require 'db.php';

$JSONstr ='{		          
                "Name":"m a ta ta",
                "RollNo":"14BME0001",
                "SubjectCode":"CEN502"
           }';
$JSONstr = file_get_contents('php://input');
class resp 
{
    function resp()
    {
        $this->Error = "0";
        $this->Message = "";
    }
}

$response = new resp();

$Obj = json_decode($JSONstr);
$RollNo = strtoupper($Obj->RollNo);
$Name = $Obj->Name;
$SubjectCode = $Obj->SubjectCode;

$query = "SELECT Semester, CBCS FROM Subjects WHERE SubjectCode = '$SubjectCode'";
$result = $conn->query($query);
$row = $result->fetch_assoc();
$Sem = $row['Semester'];
if($row['CBCS']!= 1)
{
    $response->Error = "1";
    $response->Message = "Sorry, the subject you chosen is not a CBCS subject.";
    echo json_encode($response);
    exit(0);
}

$query = "SELECT RollNo FROM StudentBase WHERE RollNo = '$RollNo'";
$result = $conn->query($query);

if($result->num_rows == 0)
{
    //new student
    $query = "INSERT INTO UserBase VALUES('$RollNo','$Name', NULL, NULL , NULL, 2 )";
    if($conn->query($query))
    {
        //inserted in userbase
    }
    else
    {
        $response->Error = "1";
        $response->Message = "Internal server error. Please try again.";
        echo json_encode($response);
        exit(0);
    }

    $strArr = explode(" ",$Name);
    if(count($strArr) == 2)
    {
        $FName = test_input($strArr[0]);
        $LName = test_input($strArr[1]);
        $query = "INSERT INTO StudentBase VALUES('$RollNo', NULL, NULL , 1, $Sem , '$FName', NULL,'$LName',NULL )";
        if($conn->query($query))
        {
           //inserted in std base
        }
        else
        {
            $qDel = "Delete FROM UserBase Where KeyString = '$RollNo'";
            $conn->query($query);
            $response->Error = "1";
            $response->Message = "Internal server error. Please try again.";
            echo json_encode($response);
            exit(0);
        }
    }
    else if(count($strArr) == 1)
    {
        $FName = test_input($strArr[0]);
        $query = "INSERT INTO StudentBase VALUES('$RollNo', NULL, NULL , 1, $Sem , '$FName', NULL,NULL,NULL )";
        if($conn->query($query))
        {
            //inserted in std base
        }
        else
        {
            $qDel = "Delete FROM UserBase Where KeyString = '$RollNo'";
            $conn->query($query);
            $response->Error = "1";
            $response->Message = "Internal server error. Please try again.";
            echo json_encode($response);
            exit(0);
        }
    }
    else
    {
        $n = count($strArr);
        $LName = test_input($strArr[$n - 1]);
        $MName = test_input($strArr[$n - 2]);
        $FName = '';

        for($i = 0;$i<$n-3;$i++)
            $FName .= test_input($strArr[$i]).' ';
        $FName .= test_input($strArr[$n-3]);

        $query = "INSERT INTO StudentBase VALUES('$RollNo', NULL, NULL , 1, $Sem , '$FName', '$MName','$LName',NULL )";
        if($conn->query($query))
        {
            //inserted in std base
        }
        else
        {
            $qDel = "Delete FROM UserBase Where KeyString = '$RollNo'";
            $conn->query($qSel);
            $response->Error = "1";
            $response->Message = "Internal server error. Please try again.";
            echo json_encode($response);
            exit(0);
        }
    }
}

$qIns = "INSERT INTO $SubjectCode (RollNo) VALUES ('$RollNo')";
$resIns = $conn->query($qIns);
if($resIns)
{
    //successful
    $query = "SELECT SubjectName FROM Subjects WHERE SubjectCode = '$SubjectCode'";
    $res = $conn->query($query);
    $row = $res->fetch_assoc();
    $Subject = $row['SubjectName'];
    $response->Message = "Successfully registered.\nName : $Name\nRoll No : $RollNo\nCBCS subject studying : $Subject";
}
else
{
    $response->Error = "1";
    $response->Message = "Not Registered. Kindly add the subject first to your subject list before admitting CBCS students.";

    $qDel = "Delete FROM UserBase Where KeyString = '$RollNo'";
    $conn->query($qDel);

    $qDel = "Delete FROM StudentBase Where RollNo = '$RollNo'";
    $conn->query($qDel);
    // internal server error
}


echo json_encode($response);

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>